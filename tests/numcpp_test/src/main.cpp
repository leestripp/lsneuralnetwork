#include <iostream>
#include <iomanip>

// utils
#include "lsDatasets.h"
#include "lsDebug.h"

// math
#include "numcpp.h"


int main( /* int argc, char *argv[] */ )
{
	cout << "TESTING : numcpp" << endl;
	cout << endl;
	
	// debug system.
	lsDebug db;
	
	// Test data
	cout << "Test data:" << endl;
	lsDatasets ds;
	auto [ X, y ] = ds.test();
	db.print( "X", X );
	db.print( "y", y );
	cout << endl;
	
	//*****************
	// numcpp
	numcpp ncp;
	vector<vector<double>> res;
	vector<vector<double>> match;
	
	// dot product : matrix.matrix
	match.push_back( vector<double> {0.05, 0.11, 0.17} );
	match.push_back( vector<double> {0.11, 0.25, 0.39} );
	match.push_back( vector<double> {0.17, 0.39, 0.61} );
	res = ncp.dot( X, ncp.transpose( X ) );
	db.print( "ncp.dot( X, ncp.transpose( X ) )", res );
	db.print( "match", match );
	ncp.match( res, match );
	cout << endl;
	
	// X * X
	match.clear();
	match.push_back( vector<double> {0.01, 0.04} );
	match.push_back( vector<double> {0.09, 0.16} );
	match.push_back( vector<double> {0.25, 0.36} );
	res = ncp.mult( X, X );
	db.print( "ncp.mult( X, X )", res );
	db.print( "match", match );
	ncp.match( res, match );
	cout << endl;
	
	// X + X
	match.clear();
	match.push_back( vector<double> {0.2, 0.4} );
	match.push_back( vector<double> {0.6, 0.8} );
	match.push_back( vector<double> {1.0, 1.2} );
	res = ncp.add( X, X );
	db.print( "ncp.add( X, X )", res );
	db.print( "match", match );
	ncp.match( res, match );
	cout << endl;
	
	// X - X
	match.clear();
	match.push_back( vector<double> {0.0, 0.0} );
	match.push_back( vector<double> {0.0, 0.0} );
	match.push_back( vector<double> {0.0, 0.0} );
	res = ncp.subtract( X, X );
	db.print( "ncp.subtract( X, X )", res );
	db.print( "match", match );
	ncp.match( res, match );
	cout << endl;
	
	return 0;
}

