project(numcpp_test LANGUAGES CXX)

include_directories(
	../shared_code/lsDatasets/
	../shared_code/
	src/
)

file(GLOB MySources
	../shared_code/lsDatasets/*.cpp
	src/*.cpp
)

add_executable(${PROJECT_NAME}
	${MySources}
)

target_link_libraries(${PROJECT_NAME}
	lsNN
)

add_dependencies(${PROJECT_NAME}
	lsNN
)
