#include "lsDatasets.h"

// numcpp
#include "numcpp.h"

lsDatasets::lsDatasets()
{
}


tuple<vector<vector<double>>, vector<ulong>> lsDatasets::spiral( ulong points, ulong classes, double spread )
{
	numcpp ncp;
	vector<vector<double>> X;
	vector<ulong> y;
	double r, t;
	
	for( ulong i=0; i<classes; i++ )
	{
		vector<double> tmp;
		for( ulong j=0; j<points; j++ )
		{
			r = double(j)/double(points);
			t = i*4 + (4*r);
			// x, y positions.
			tmp.push_back( (r * cos( t * 2.5 )) + ( ncp.random( -spread, spread ) ) );
			tmp.push_back( (r * sin( t * 2.5 )) + ( ncp.random( -spread, spread ) ) );
		}
		X.push_back( tmp );
		y.push_back( i );
	}
    return make_tuple( X, y );
}

// large numbers
// right, wrong test.

tuple<vector<vector<double>>, vector<ulong>> lsDatasets::large_numbers( ulong num_tokens )
{
	random_device dev;
	mt19937 rng(dev());
	uniform_int_distribution<mt19937::result_type> dist( 1, 1000 );
	
	vector<vector<double>> X;
	vector<ulong> y;
	
	vector<double> data_main;
	vector<double> data;
	if( num_tokens < 8 ) num_tokens = 8;
	// Goal numbers/pattern.
	ulong v = 1000 / num_tokens;
	for( ulong i=0; i<num_tokens; i++ )
	{
		data_main.push_back( i*v );
	}
	
	// make block of data.
	for( ulong i=0; i<100; i++ )
	{
		data.clear();
		// random noise
		for( ulong i=0; i<num_tokens; i++ )
		{
			data.push_back(  dist(rng) );
		}
		// main data.
		X.push_back( data_main );
		y.push_back( 0 );
		// random
		X.push_back( data );
		y.push_back( 1 );
	}
	
    return make_tuple( X, y );
}

// numcpp testing

tuple<vector<vector<double>>, vector<ulong>> lsDatasets::test()
{
	vector<vector<double>> X;
	vector<ulong> y;
	
	X.push_back( vector<double> { 0.1, 0.2 } );
	X.push_back( vector<double> { 0.3, 0.4 } );
	X.push_back( vector<double> { 0.5, 0.6 } );
	
	y.push_back( 0 );
	y.push_back( 1 );
	y.push_back( 2 );
	
    return make_tuple( X, y );
}

