#ifndef LSDATASETS_H
#define LSDATASETS_H

#include <tuple>
#include <iterator>
#include <vector>
#include <numeric>
#include <iostream>
#include <algorithm>
#include <cmath>
#include <random>

using namespace std;

class lsDatasets
{
public:
	lsDatasets();
	
	// datasets
	tuple<vector<vector<double>>, vector<ulong>> spiral( ulong points, ulong classes, double spread=0.07 );
	tuple<vector<vector<double>>, vector<ulong>> large_numbers( ulong num_tokens );
	
	// testing
	tuple<vector<vector<double>>, vector<ulong>> test();
};

#endif // LSDATASETS_H
