#include "myApp.h"

// datasets
#include "lsDatasets.h"

myApp::myApp() :
	m_vBox( Gtk::Orientation::VERTICAL ),
	m_ButtonBox( Gtk::Orientation::HORIZONTAL ),
	// controls
	m_label_iter( "Iterations", Gtk::Align::END ),
	m_label_neurons( "Hidden Neurons", Gtk::Align::END ),
	m_label_dropout( "Dropout factor", Gtk::Align::END ),
	m_label_LR( "Learning rate", Gtk::Align::END ),
	m_label_Decay( "Decay", Gtk::Align::END ),
	m_label_Momentum( "Momentum", Gtk::Align::END ),
	// Buttons
	m_Button_ViewData( "View Data" ),
	m_Button_Defaults( "Defaults" ),
	m_Button_Learn( "Learn" ),
	m_Button_Train( "Train" ),
	m_Button_Stop( "Stop" ),
	m_Button_Predict( "Predict" ),
	m_Button_Save( "Save" )
{
	set_title( "lsNN test model" );
	set_default_size( 800, 820 );
	
	// number of tokens to use.
	m_num_tokens = 4;
	
	// layout
	m_vBox.set_expand();
	m_vBox.append( m_graph );
	m_vBox.append( m_grid_values );
	m_vBox.append( m_ButtonBox );
	m_vBox.set_margin( 5 );
	
	// buttons
	m_ButtonBox.set_hexpand( true );
	m_ButtonBox.append( m_Button_ViewData );
	m_ButtonBox.append( m_Button_Defaults );
	m_ButtonBox.append( m_Button_Learn );
	m_ButtonBox.append( m_Button_Train );
	m_ButtonBox.append( m_Button_Stop );
	m_ButtonBox.append( m_Button_Predict );
	m_ButtonBox.append( m_Button_Save );
	m_ButtonBox.set_margin( 5 );
	
	// value grid : col - row
	m_grid_values.set_hexpand( true );
	m_grid_values.set_margin( 5 );
	m_grid_values.attach( m_label_iter, 0, 0);
	m_grid_values.attach( m_spin_iter, 1, 0);
	m_grid_values.attach( m_label_neurons, 2, 0);
	m_grid_values.attach( m_spin_neurons, 3, 0);
	m_grid_values.attach( m_label_dropout, 4, 0);
	m_grid_values.attach( m_spin_dropout, 5, 0);
	// row 2
	m_grid_values.attach( m_label_LR, 0, 1);
	m_grid_values.attach( m_spin_LR, 1, 1);
	m_grid_values.attach( m_label_Decay, 2, 1);
	m_grid_values.attach( m_spin_Decay, 3, 1);
	m_grid_values.attach( m_label_Momentum, 4, 1);
	m_grid_values.attach( m_spin_Momentum, 5, 1);
	
	// Adjustments.
	m_adjustment_iter = Gtk::Adjustment::create( 100.0, 1.0, 100000.0, 10.0, 100.0, 0.0 );
	m_adjustment_neurons = Gtk::Adjustment::create( 4.0, 1.0, 100000.0, 10.0, 100.0, 0.0 );
	m_adjustment_dropout = Gtk::Adjustment::create( 0.1, 0.0, 1.0, 0.1, 0.01, 0.0 );
	m_adjustment_LR = Gtk::Adjustment::create( 0.001, 0.00001, 2.0, 0.01, 0.001, 0.0 );
	m_adjustment_Decay = Gtk::Adjustment::create( 0.001, 0.00001, 1.0, 0.00001, 0.001, 0.0 );
	m_adjustment_Momentum = Gtk::Adjustment::create( 0.9, 0.01, 1.0, 0.1, 0.1, 0.0 );
	// set adjustments.
	m_spin_iter.set_adjustment( m_adjustment_iter );
	m_spin_neurons.set_adjustment( m_adjustment_neurons );
	m_spin_dropout.set_adjustment( m_adjustment_dropout );
	m_spin_dropout.set_digits( 2 );
	m_spin_LR.set_adjustment( m_adjustment_LR );
	m_spin_LR.set_digits( 4 );
	m_spin_Decay.set_adjustment( m_adjustment_Decay );
	m_spin_Decay.set_digits( 10 );
	m_spin_Momentum.set_adjustment( m_adjustment_Momentum );
	m_spin_Momentum.set_digits( 2 );
	
	set_child( m_vBox );
	
	//*******************************
	// Create and Copy training data
	// Load vocab
	m_tokenizer.load_vocab( "vocab.lsv" );
	
	// data
	string data = m_tokenizer.load( "../test_data/pod_42.txt" );
	// string data = m_tokenizer.load( "../test_data/War_of_the_Worlds.txt" );
	// cout << "### Data :\n" << data << endl;
	// turn text into tokens.
	vector<string> tokens = m_tokenizer.tokenize( data, true );		// true = tolower.
	if( tokens.empty() )
	{
		cerr << "ERROR: No tokens found in data." << endl;
	}
	// Create tokenID's..
	m_tokenizer.insert_tokens( tokens );
	
	// Split tokens up into blocks.
	vector<vector<string>> token_list = m_tokenizer.split_tokens( tokens, 128 );
	if( token_list.empty() )
	{
		cerr << "ERROR: Could not split tokens into blocks." << endl;
	}
	
	// add first block to context for testing.
	m_context.m_buffer = m_tokenizer.toContext( token_list[0], false );	// true = Add [EOS]
	// debug
	m_context.print();
	cout << endl;
	// gen data
	auto[ X, y ] = m_context.gen_data( m_num_tokens );
	// store
	m_X = X;
	m_y = y;
	// debug
	m_debug.print( "m_X", m_X );
	m_debug.print( "m_y", m_y );
	m_predict_pos = 0;
	
	// calc graph zoom
	m_zoom_factor = 1.0;
	for( const auto& vec : m_X )
	{
		double tmp = m_ncp.find_max( vec );
		if( tmp > m_zoom_factor ) m_zoom_factor = tmp;
	}
	
	// Action Signals
	m_Button_ViewData.signal_clicked().connect( sigc::mem_fun(*this, &myApp::on_action_viewdata ) );
	m_Button_Defaults.signal_clicked().connect( sigc::mem_fun(*this, &myApp::on_action_defaults ) );
	m_Button_Learn.signal_clicked().connect( sigc::mem_fun(*this, &myApp::on_action_learn ) );
	m_Button_Train.signal_clicked().connect( sigc::mem_fun(*this, &myApp::on_action_train ) );
	m_Button_Stop.signal_clicked().connect( sigc::mem_fun(*this, &myApp::on_action_stop ) );
	m_Button_Predict.signal_clicked().connect( sigc::mem_fun(*this, &myApp::on_action_predict ) );
	m_Button_Save.signal_clicked().connect( sigc::mem_fun(*this, &myApp::on_action_save ) );
	
	// Model defaults
	m_model.m_settings.m_mode = LSM_TRAIN_VALIDATE;
	m_model.m_settings.m_iterations = 100;
	m_model.m_settings.m_hidden_neurons = 4;
	m_model.m_settings.m_dropout_factor = 0.1;
	m_model.m_settings.m_learning_rate = 0.0001;
	m_model.m_settings.m_decay = 0.001;
	m_model.m_settings.m_momentum = 0.9;
	// Set our thread type, for later if we need to Cast it.
	// this value is only for the dev to Identify what thread sent the notify signal.
	m_model.set_type( LSTT_MODEL );
	
	// Model
	m_model.m_settings.m_model_filename = "model.lsm";
	m_model.m_settings.m_epoch_update = 10;
	
	// Model connect signals
	m_model.notify_update.connect( sigc::mem_fun( *this, &myApp::notify_update) );
	m_model.notify_done.connect( sigc::mem_fun( *this, &myApp::notify_done) );
}

//***************
// Run

void myApp::run()
{
	// Start the models thread.
	m_model.start();
}

void myApp::view_data()
{
	// graph data
	m_graph.set_data( m_X );
	m_graph.set_draw_style( LSDS_POINTS );
	m_graph.set_data_type( LSGDT_XY );
	m_graph.set_data_scale( 1.0 / m_zoom_factor );
	m_graph.queue_draw();
}

//****************
// Action Slots

void myApp::on_action_viewdata()
{
	// sets data and triggers redraw.
	view_data();
}

void myApp::on_action_defaults()
{
	m_predict_pos = 0;
	
	// Set defaults
	m_spin_iter.set_value( 3000 );
	m_spin_neurons.set_value( 64 );
	m_spin_dropout.set_value( 0.02 );
	m_spin_LR.set_value( 0.001 );
	m_spin_Decay.set_value( 0.001 );
	m_spin_Momentum.set_value( 0.9 );
}

// Learn

void myApp::on_action_learn()
{
	m_predict_pos = 0;
	
	// copy data
	m_model.set_X( m_X );
	m_model.set_y( m_y );
	m_model.set_test_X( m_test_X );
	m_model.set_test_y( m_test_y );
	// mode.
	m_model.m_settings.m_mode = LSM_LEARN;
	
	// Get settings from gui
	// Learn sould only change some values.
	m_model.m_settings.m_iterations = (ulong) m_spin_iter.get_value();
	
	run();
}

void myApp::on_action_train()
{
	m_predict_pos = 0;
	
	// copy data
	m_model.set_X( m_X );
	m_model.set_y( m_y );
	m_model.set_test_X( m_test_X );
	m_model.set_test_y( m_test_y );
	// mode.
	m_model.m_settings.m_mode = LSM_TRAIN_VALIDATE;
	
	// Get settings from gui
	m_model.m_settings.m_iterations = (ulong) m_spin_iter.get_value();
	m_model.m_settings.m_hidden_neurons = (ulong) m_spin_neurons.get_value();
	m_model.m_settings.m_dropout_factor = m_spin_dropout.get_value();
	m_model.m_settings.m_learning_rate = m_spin_LR.get_value();
	m_model.m_settings.m_decay = m_spin_Decay.get_value();
	m_model.m_settings.m_momentum = m_spin_Momentum.get_value();
	
	run();
}

void myApp::on_action_stop()
{
	m_predict_pos = 0;
	
	m_model.set_stop( true );
}

void myApp::on_action_predict()
{
	m_predict_pos++;
	if( m_predict_pos > m_context.m_buffer.size() ) m_predict_pos = 0;
	
	// Predict
	auto pX = m_context.gen_predict_data( m_predict_pos, m_num_tokens );
	m_debug.print( "predict X", pX );
	m_model.set_X( pX );
	// Mode
	m_model.m_settings.m_mode = LSM_PREDICT;
	
	run();
}

void myApp::on_action_save()
{
	// Save vocab
	m_tokenizer.save_vocab( "vocab.lsv" );
	// Save model
	m_model.save();
}

//****************
// model Slots

void myApp::notify_update( lsThread *model )
{
	// debug
	// cout << "Signal: notify_update from model" << endl;
	
	switch( model->get_type() )
	{
		case LSTT_MODEL:
		{
			// debug
			// cout << "Thread type: LSTT_MODEL" << endl;
			
			lsModel *m = (lsModel *)model;
			m_graph.set_data( m->get_stats() );
			m_graph.set_draw_style( LSDS_LINES );
			m_graph.set_data_type( LSGDT_X );
			m_graph.set_data_scale( 1.0 );
			m_graph.queue_draw();
			break;
		}
		
		case LSTT_NONE:
		{
			cout << "Thread type: LSTT_NONE" << endl;
			break;
		}
		
		default:
		{
			cout << "Unknown Thread type..." << endl;
			break;
		}
	}
}

void myApp::notify_done( lsThread *model )
{
	// debug
	// cout << "Signal: notify_done from model" << endl;
	
	switch( model->get_type() )
	{
		case LSTT_MODEL:
		{
			// debug
			// cout << "Thread type: LSTT_MODEL" << endl;
			
			lsModel *m = (lsModel *)model;
			m_graph.set_data( m->get_stats() );
			m_graph.set_draw_style( LSDS_LINES );
			m_graph.set_data_type( LSGDT_X );
			m_graph.set_data_scale( 1.0 );
			m_graph.queue_draw();
			// predictions
			switch( m->m_settings.m_mode )
			{
				case LSM_PREDICT:
				{
					trieNode *node = m_tokenizer.find_node( m->m_class );
					if( node )
					{
						cout << "Prediction class : " << m->m_class << endl;
						cout << "Lookup token     : " << m_tokenizer.find_token( m->m_class ) << endl;
						cout << "Previous tokens  : ";
						for( const ulong& ID : node->m_prev_tokenID_list )
						{
							cout << m_tokenizer.find_token( ID ) << " ";
						}
						cout << endl;
					} else
					{
						cout << "No prediction found..." << endl;
					}
					break;
				}
			}
			break;
		}
		
		case LSTT_NONE:
		{
			cout << "Thread type: LSTT_NONE" << endl;
			break;
		}
		
		default:
		{
			cout << "Unknown Thread type..." << endl;
			break;
		}
	}
}
