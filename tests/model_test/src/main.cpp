#include "myApp.h"

int main( int argc, char *argv[] )
{
	cout << "TESTING : lsModel" << endl;
	auto app = Gtk::Application::create( "com.leestripp.lsmodel_test" );
	return app->make_window_and_run<myApp>( argc, argv );
}

