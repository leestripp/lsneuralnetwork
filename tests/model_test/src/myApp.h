#ifndef MYAPP_H
#define MYAPP_H

#include <iostream>

// gtkmm
#include <gtkmm.h>

// thread
#include "lsThread.h"
// model
#include "lsModel.h"
// graph
#include "lsGraph.h"
// numcpp
#include "numcpp.h"
// debug
#include "lsDebug.h"
// lsTokenizer
#include "lsTokenizer.h"
// lsContext
#include "lsContext.h"

// Thread types
const int LSTT_NONE		= 0;
const int LSTT_MODEL	= 1;

using namespace std;

class myApp : public Gtk::Window
{
public:
	myApp();
	
	void run();
	void view_data();
	
	// model Slots
	void notify_update( lsThread *model );
	void notify_done( lsThread *model );
	
	// data
	vector<vector<double>> m_X;
	vector<ulong> m_y;
	// test data
	vector<vector<double>> m_test_X;
	vector<ulong> m_test_y;
	
private:
	lsModel m_model;
	
	ulong m_num_tokens;
	
	// Action slots
	void on_action_viewdata();
	void on_action_defaults();
	void on_action_learn();
	void on_action_train();
	void on_action_stop();
	void on_action_predict();
	void on_action_save();
	
	// widgets
	Gtk::Box m_vBox;
	Gtk::Box m_ButtonBox;
	Gtk::Grid m_grid_values;
	// labels and values
	Gtk::Label m_label_iter;
	Gtk::SpinButton m_spin_iter;
	Gtk::Label m_label_neurons;
	Gtk::SpinButton m_spin_neurons;
	Gtk::Label m_label_dropout;
	Gtk::SpinButton m_spin_dropout;
	Gtk::Label m_label_LR;
	Gtk::SpinButton m_spin_LR;
	Gtk::Label m_label_Decay;
	Gtk::SpinButton m_spin_Decay;
	Gtk::Label m_label_Momentum;
	Gtk::SpinButton m_spin_Momentum;
	// Buttons
	Gtk::Button m_Button_ViewData;
	Gtk::Button m_Button_Defaults;
	Gtk::Button m_Button_Learn;
	Gtk::Button m_Button_Train;
	Gtk::Button m_Button_Stop;
	Gtk::Button m_Button_Predict;
	Gtk::Button m_Button_Save;
	
	// lsGraph
	lsGraph m_graph;
	double m_zoom_factor;
	
	// adjustments
	Glib::RefPtr<Gtk::Adjustment> m_adjustment_iter;
	Glib::RefPtr<Gtk::Adjustment> m_adjustment_neurons;
	Glib::RefPtr<Gtk::Adjustment> m_adjustment_dropout;
	Glib::RefPtr<Gtk::Adjustment> m_adjustment_LR;
	Glib::RefPtr<Gtk::Adjustment> m_adjustment_Decay;
	Glib::RefPtr<Gtk::Adjustment> m_adjustment_Momentum;
	
	// predict
	ulong m_predict_pos;
	
	// data
	lsTokenizer m_tokenizer;
	lsContext m_context;
	// numcpp
	numcpp m_ncp;
	// debug
	lsDebug m_debug;
};

#endif // MYAPP_H
