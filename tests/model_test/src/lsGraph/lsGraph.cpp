#include "lsGraph.h"

lsGraph::lsGraph()
{
	// Crash without this. DO NOT midify.
	set_size_request( 100, 100 );
	set_expand();
	
	// css name.
	set_name( "lsGraph" );
	Glib::ustring data = ".lsGraph {background-color: #010101;}";
	auto provider = Gtk::CssProvider::create();
	provider->load_from_data( data );
	auto ctx = get_style_context();
	ctx->add_class( "lsGraph" );
	ctx->add_provider( provider, GTK_STYLE_PROVIDER_PRIORITY_USER );
	
	// draw
	m_draw_style = LSDS_LINES;
	m_data_scale = 1.0;
	set_draw_func( sigc::mem_fun(*this, &lsGraph::on_draw) );
}

// set data.

void lsGraph::set_data( const vector<vector<double>>& data )
{
	m_data_mutex.lock();
	m_data = data;
	m_data_mutex.unlock();
}

//***************
// Draw func

void lsGraph::on_draw( const Cairo::RefPtr<Cairo::Context>& cr, int width, int height )
{
	// store context
	m_cr = cr;
	m_width = width;
	m_height = height;
	
	// set scale and translate to (1.0, 1.0) to be the center of the widget.
	// x/y -1.0 <-> 1.0, also need to flip Y
	m_cr->scale( m_width * 0.5, -(m_height * 0.5) );
	m_cr->translate( 1.0, -1.0 );
	m_cr->set_line_width( 0.001 );
	
	// draw grid.
	draw_grid();
	
	// No data, just return.
	if( m_data.empty() ) return;
	
	// draw data
	m_data_mutex.lock();
	switch( m_draw_style )
	{
		case LSDS_POINTS:
		{
			draw_points();
			break;
		}
		
		case LSDS_LINES:
		{
			draw_lines();
			break;
		}
	}
	m_data_mutex.unlock();
}

//***************
// Grid

void lsGraph::draw_grid()
{
	m_cr->set_line_width( 0.001 );
	
	// Draw grid.
	m_cr->set_source_rgba( 0.1, 0.1, 0.1, 1.0 );
	for( double i=-1.0; i<=1.0; i+=0.1 )
	{
		// X axis lines
		m_cr->move_to( -1.0, i );
		m_cr->line_to( 1.0, i );
		// Y axis lines
		m_cr->move_to( i, -1.0 );
		m_cr->line_to( i, 1.0 );
	}
	m_cr->stroke();
	
	// Draw main lines - Red
	m_cr->set_source_rgba( 0.8, 0.1, 0.1, 1.0 );
	m_cr->move_to( -1.0, 0.0 );
	m_cr->line_to( 1.0, 0.0 );
	// Y axis - Green
	m_cr->set_source_rgba( 0.1, 0.8, 0.1, 1.0 );
	m_cr->move_to( 0.0, -1.0 );
	m_cr->line_to( 0.0, 1.0 );
	
	m_cr->stroke();
}

//***************
// Styles

void lsGraph::draw_points()
{
	m_cr->set_line_width(0.002);
	
	// Rows to display
	ulong num_rows = m_data.size();
	
	// data type
	switch( m_data_type )
	{
		case LSGDT_NONE:
		case LSGDT_X:
		{
			// data columns
			for( ulong col=0; col<m_data[0].size(); col++ )
			{
				// Random colours
				m_cr->set_source_rgba( m_ncp.random(0.5,1.0), m_ncp.random(0.5,1.0), m_ncp.random(0.5,1.0), 0.3 );
				
				double x = -1.0;	// -1 : 1
				double x_inc = (1.0 / (double)num_rows ) * 2.0;
				for( ulong row=0; row<num_rows; row++ )
				{
					// point
					m_cr->save();
					m_cr->arc( x, m_data[row][col] * m_data_scale, 0.003, 0.0, 2.0 * M_PI );
					m_cr->fill_preserve();
					m_cr->restore();
					m_cr->stroke();
					
					x += x_inc;
				}
			}
			break;
		}
		
		case LSGDT_XY:
		{
			// data rows
			for( const auto& row : m_data )
			{
				// Random colours
				m_cr->set_source_rgba( m_ncp.random(0.5,1.0), m_ncp.random(0.5,1.0), m_ncp.random(0.5,1.0), 0.3 );
				
				// x,y pairs
				for( ulong i=0; i<row.size(); i+=2 )
				{
					// point
					m_cr->save();
					m_cr->arc( row[i] * m_data_scale, row[i+1] * m_data_scale, 0.003, 0.0, 2.0 * M_PI );
					m_cr->fill_preserve();
					m_cr->restore();
					m_cr->stroke();
				}
			}
			break;
		}
		
		default:
		{
			cerr << "ERROR: lsGraph : Unknown data type." << endl;
			break;
		}
	} // switch
}

void lsGraph::draw_lines()
{
	m_cr->set_line_width( 0.002 );
	
	// Rows to display
	ulong num_rows = m_data.size();
	
	// data columns
	for( ulong col=0; col<m_data[0].size(); col++ )
	{
		// Random colours
		m_cr->set_source_rgba( m_ncp.random(0.5,1.0), m_ncp.random(0.5,1.0), m_ncp.random(0.5,1.0), 0.80 );
		
		double x = -1.0;	// -1 : 1
		double x_inc = (1.0 / (double)num_rows ) * 2.0;
		for( ulong row=0; row<num_rows; row++ )
		{
			if( row == 0 )
			{
				m_cr->move_to( x, m_data[row][col] * m_data_scale );
			} else
			{
				m_cr->line_to( x, m_data[row][col] * m_data_scale );
			}
			x += x_inc;
		}
		m_cr->stroke();
	}
}
