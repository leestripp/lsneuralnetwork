#ifndef LSGRAPH_H
#define LSGRAPH_H

#include <iostream>
#include <vector>
#include <thread>

// gtkmm
#include <gtkmm/widget.h>
#include <gtkmm/cssprovider.h>
#include <cairomm/context.h>
#include <glibmm/main.h>
#include <gtkmm/drawingarea.h>

// numcpp
#include "numcpp.h"

// Draw style
const ulong LSDS_POINTS		= 0;
const ulong LSDS_LINES		= 1;
// Data types
const ulong LSGDT_NONE	= 0;
const ulong LSGDT_X		= 1;
const ulong LSGDT_XY	= 2;

using namespace std;

class lsGraph : public Gtk::DrawingArea
{
public:
	lsGraph();
	
	void set_data( const vector<vector<double>>& data );
	
	// get set
	void set_draw_style( ulong val )
	{
		m_draw_style = val;
	}
	
	void set_data_type( ulong val )
	{
		m_data_type = val;
	}
	
	void set_data_scale( double val )
	{
		m_data_scale = val;
	}
	
private:
	void on_draw( const Cairo::RefPtr<Cairo::Context>& cr, int width, int height );
	
	// grid
	void draw_grid();
	// styles
	void draw_points();
	void draw_lines();
	ulong m_draw_style;
	ulong m_data_type;
	double m_data_scale;
	
	// context
	Cairo::RefPtr<Cairo::Context> m_cr;
	int m_width;
	int m_height;
	
	// css
	Glib::RefPtr<Gtk::CssProvider> m_refCssProvider;
	
	// data
	vector<vector<double>> m_data;
	mutex m_data_mutex;
	
	// numcpp
	numcpp m_ncp;
};

#endif // LSGRAPH_H
