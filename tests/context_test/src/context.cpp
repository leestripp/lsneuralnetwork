#include <iostream>
#include <string>
#include <vector>

// lsDebug
#include "lsDebug.h"
// lsTokenizer
#include "lsTokenizer.h"
// lsContext
#include "lsContext.h"


int main()
{
	lsTokenizer tokenizer;
	lsDebug debug;
	
	// Load vocab
	tokenizer.load_vocab( "vocab.lsv" );
	
	// data
	// string data = "Hello, my name is Bob!";
	string data = tokenizer.load( "../test_data/War_of_the_Worlds.txt" );
	cout << "Data: " << data << endl;
	
	// turn text into tokens.
	vector<string> tokens = tokenizer.tokenize( data, true );		// true = tolower.
	if( tokens.empty() )
	{
		cerr << "ERROR: No tokens found in data." << endl;
		return 1;
	}
	
	// Create tokenID's.
	tokenizer.insert_tokens( tokens );
	
	// debug
	// tokenizer.print();
	// cout << endl;
	
	cout << "\nContext ----------" << endl;
	lsContext context;
	
	// add to context
	context.m_buffer = tokenizer.toContext( tokens, true );		// true = Add [EOS]
	// debug
	context.print();
	cout << endl;
	// Save vocab after convert as it adds links to previous tokens.
	tokenizer.save_vocab( "vocab.lsv" );
	
	ulong num_tokens = 8;
	
	cout << "Train X, y ----------" << endl;
	
	auto[ X, y ] = context.gen_data( num_tokens );
	if( X.empty() ) return 1;
	// debug
	debug.print( "X", X );
	debug.print( "y", y );
	
	cout << "Predict X ----------" << endl;
	
	auto pX = context.gen_predict_data( context.m_buffer.size()-2, num_tokens );
	debug.print( "predict X", pX );
	
	return 0;
}

