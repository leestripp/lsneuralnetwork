#include "lsNeuralNetwork.h"

// layers
#include "lsLayer_Base.h"
#include "lsLayer_dense.h"
#include "lsLayer_dropout.h"
// activations
#include "lsActivation_Base.h"
// optimizers
#include "lsOptimizer_SGD.h"


lsNeuralNetwork::lsNeuralNetwork()
{
	m_layer_list = NULL;
	m_last_layer = NULL;
	m_optimizer = NULL;
}

lsNeuralNetwork::~lsNeuralNetwork()
{
	if( m_optimizer ) delete m_optimizer;
	
	if( m_layer_list )
	{
		// each layer deletes the next layer.
		delete m_layer_list;
	}
}

void lsNeuralNetwork::add_layer( lsLayer_Base *layer )
{
	if(! m_last_layer )
	{
		// Add first layer.
		m_layer_list = layer;
		m_last_layer = layer;
		
	} else
	{
		// Add to end.
		m_last_layer->m_next_layer = layer;
		layer->m_prev_layer = m_last_layer;
		m_last_layer = layer;
	}
}

lsLayer_Base* lsNeuralNetwork::get_Last_Layer()
{
	return m_last_layer;
}

// Find best prediction in last layer outputs.

ulong lsNeuralNetwork::get_class()
{
	return m_ncp.argmax( m_last_layer->m_outputs[0] );
}


//**************************
// forward, backward.

tuple<double, double> lsNeuralNetwork::forward()
{
	double loss = 1.0;
	double acc = 0.0;
	
	if(! m_layer_list )
	{
		cerr << "ERROR: lsModel::forward() : No layers in network." << endl;
		return make_tuple( loss, acc );
	}
	
	// check pass..
	switch( m_last_layer->m_activation->m_activation_type )
	{
		case LSAT_Softmax_LossCCE:
		{
			// Train, learn.
			m_last_layer->m_y = m_y;
			m_layer_list->forward( m_X );
			
			loss = m_last_layer->m_loss;
			acc = m_ncp.accuracy( m_last_layer->m_outputs, m_y );
			break;
		}
		
		case LSAT_Softmax:
		{
			// Predict network.
			m_last_layer->m_y.clear();
			m_layer_list->forward( m_X );
			break;
		}
	}
	
	return make_tuple( loss, acc );
}

void lsNeuralNetwork::backward()
{
	if(! m_last_layer )
	{
		cerr << "ERROR: lsModel::backward() : No layers in network." << endl;
		return;
	}
	
	// Traverse network. m_y set in forward pass.
	m_last_layer->backward( m_last_layer->m_outputs );
}

void lsNeuralNetwork::optimize()
{
	m_optimizer->pre_update_params();
	lsLayer_Base *layer = m_layer_list;
	while( layer )
	{
		if( layer->m_layer_type == LSLT_DENSE )
		{
			lsLayer_dense *tmp_layer = (lsLayer_dense *)layer;
			m_optimizer->update_params( tmp_layer );
		}
		// Next layer.
		layer = layer->m_next_layer;
	}
	m_optimizer->post_update_params();
}

void lsNeuralNetwork::set_activation_function( ulong af )
{
	// make sure we have a layer to set.
	if(! m_last_layer ) return;
	
	if( m_last_layer->m_layer_type == LSLT_DENSE )
	{
		lsLayer_dense *layer = (lsLayer_dense *)m_last_layer;
		// Set
		layer->set_activation( af );
	}
}

void lsNeuralNetwork::set_dropout_enabled( bool enabled )
{
	lsLayer_Base *layer = m_layer_list;
	while( layer )
	{
		if( layer->m_layer_type == LSLT_DROPOUT )
		{
			lsLayer_dropout *tmp_layer = (lsLayer_dropout *)layer;
			tmp_layer->m_enabled = enabled;
		}
		// Next layer.
		layer = layer->m_next_layer;
	}
}


string lsNeuralNetwork::save()
{
	stringstream output;
	
	// collect trained model data.
	output << "# Neural Network data, weights and biases." << endl;
	
	// collect layer data.
	lsLayer_Base *layer = m_layer_list;
	while( layer )
	{
		output << layer->save();
		
		// Next layer.
		layer = layer->m_next_layer;
	}
	return output.str();
}


