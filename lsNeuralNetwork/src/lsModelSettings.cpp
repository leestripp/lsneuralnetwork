#include "lsModelSettings.h"

lsModelSettings::lsModelSettings()
{
	// mode
	m_mode = LSM_NONE;
	
	// filename.
	m_model_filename = "model.lsm";
	
	// main loop
	m_iterations = 1000;
	m_epoch_update = 100;
	// optimizer
	m_learning_rate = 1.0;
	m_decay = 1.0e-03;
	m_momentum = 0.9;
	// hidden layers
	m_hidden_neurons = 4;
	// Dropout
	m_dropout_factor = 0.1;
	
	// training info
	m_accuracy = 0.0;
	m_loss = 1.0;
	m_current_iterations = 0;
}


string lsModelSettings::save()
{
	stringstream settings;
	
	settings << "# lsModel v0.2a" << endl;
	// dataset info
	settings << "# Training information" << endl;
	settings << "# m_accuracy " << format( "{}", m_accuracy );
	settings << endl;
	settings << "# m_loss " << format( "{}", m_loss );
	settings << endl;
	
	settings << "current_iterations " << format( "{}", m_current_iterations );
	settings << endl;
	
	// Training settings.
	settings << "# Training settings" << endl;
	settings << "iterations " << format( "{}", m_iterations );
	settings << endl;
	settings << "hidden_neurons " << format( "{}", m_hidden_neurons );
	settings << endl;
	settings << "dropout_factor " << format( "{}", m_dropout_factor );
	settings << endl;
	settings << "learning_rate " << format( "{}", m_learning_rate );
	settings << endl;
	settings << "decay " << format( "{}", m_decay );
	settings << endl;
	settings << "momentum " << format( "{}", m_momentum );
	
	return settings.str();
}

void lsModelSettings::add_iterations( ulong val )
{
	m_current_iterations += val;
	// Limit iterations, so we can reset or remove models.
	if( m_current_iterations > 10000000 )
	{
		m_current_iterations = 10000000;
	}
}

// debug

void lsModelSettings::print()
{
	cout << "m_current_iterations : " << m_current_iterations << endl;
	cout << "m_model_filename : " << m_model_filename << endl;
	cout << "m_iterations : " << m_iterations << endl;
	cout << "m_epoch_update : " << m_epoch_update << endl;
	cout << "m_learning_rate : " << m_learning_rate << endl;
	cout << "m_decay : " << m_decay << endl;
	cout << "m_momentum : " << m_momentum << endl;
	cout << "m_hidden_neurons : " << m_hidden_neurons << endl;
	cout << "m_dropout_factor : " << m_dropout_factor << endl;
	
	cout << "m_accuracy : " << m_accuracy << endl;
	cout << "m_loss : " << m_loss << endl;
}
