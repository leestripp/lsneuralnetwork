#ifndef LSMODEL_H
#define LSMODEL_H

#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
#include <filesystem>
#include <cstdio>
#include <vector>
#include <thread>
#include <mutex>
// user home
#include <unistd.h>
#include <pwd.h>

// Settings.
#include "lsModelSettings.h"

// numcpp
#include "numcpp.h"
// debug
#include "lsDebug.h"
// thread
#include "lsThread.h"

// protos
class lsNeuralNetwork;

using namespace std;

class lsModel : public lsThread
{
public:
	lsModel();
	virtual ~lsModel();
	
	void work();
	void learn();
	void train();
	void predict();
	
	// data set get mutex protected.
	void set_X( const vector<vector<double>>& X );
	void get_X( vector<vector<double>>& X );
	void set_y( const vector<ulong>& y );
	void get_y( vector<ulong>& y );
	void clear_data();
	// test data
	void set_test_X( const vector<vector<double>>& X );
	void get_test_X( vector<vector<double>>& X );
	void set_test_y( const vector<ulong>& y );
	void get_test_y( vector<ulong>& y );
	void clear_test_data();
	
	// save, load and delete model
	// m_settings holds the filename to use.
	void save();
	void load();
	void delete_model();
	
	// get set
	void set_stop( bool val )
	{
		m_stop = val;
	}
	
	const string& get_model_dir()
	{
		return m_model_dir;
	}
	void set_model_dir( const string& val )
	{
		m_model_dir = val;
		cout << "Model dir : " << m_model_dir << endl;
	}
	
	// Settings.
	lsModelSettings m_settings;
	
	// Store outputs
	vector<double> m_outputs;
	ulong m_class;
	
	// Utilities
	vector<string> split( string str, const string& token );
	
	// debug.
	vector<vector<double>> get_stats();
	
private:
	// network.
	lsNeuralNetwork *m_neuralnet;
	// data
	vector<vector<double>> m_X;
	vector<ulong> m_y;
	// test data
	vector<vector<double>> m_test_X;
	vector<ulong> m_test_y;
	mutex m_data_mutex;
	
	// stats
	void add_stats( const vector<double>& stats );
	mutex m_stats_mutex;
	vector<vector<double>> m_stats;
	
	// paths
	string m_model_dir;
	
	// Thread stop
	bool m_stop;
	
	// numcpp
	numcpp m_ncp;
	// debug
	lsDebug m_db;
};


#endif // LSMODEL_H