#include "lsOptimizer_SGD.h"

// layer
#include "lsLayer_dense.h"


lsOptimizer_SGD::lsOptimizer_SGD( double learning_rate, double decay, double momentum )
{
	m_learning_rate = learning_rate;
	m_current_learning_rate = m_learning_rate;
	m_decay = decay;
	m_iterations = 0;
	m_momentum = momentum;
}

lsOptimizer_SGD::~lsOptimizer_SGD()
{
}

void lsOptimizer_SGD::pre_update_params()
{
	if( m_decay != 0.0 )
	{
		m_current_learning_rate = m_learning_rate * ( 1.0 / ( 1.0 + m_decay * m_iterations ) );
	}
}

void lsOptimizer_SGD::update_params( lsLayer_dense *layer )
{
	// debug
	// cout << "lsOptimizer_SGD::update_params" << endl;
	
	// If we use momentum
	if( m_momentum != 0.0 )
	{
		// weight_updates = self.momentum * layer.weight_momentums - self.current_learning_rate * layer.dweights
		vector<vector<double>> wu1, wu2;
		// self.momentum * layer.weight_momentums
		wu1 = m_ncp.mult( layer->m_weight_momentums, m_momentum );
		// self.current_learning_rate * layer.dweights
		wu2 = m_ncp.mult( layer->m_dweights, m_current_learning_rate );
		// layer.weight_momentums = weight_updates
		layer->m_weight_momentums = m_ncp.subtract(  wu1, wu2 );
		// layer.weights += weight_updates
		layer->m_weights = m_ncp.add( layer->m_weights, layer->m_weight_momentums );
		// debug
		// m_db.print( "layer->m_weights", layer->m_weights );
		
		// Build bias updates
		vector<double> bu1, bu2;
		// bias_updates = self.momentum * layer.bias_momentums - self.current_learning_rate * layer.dbiases
		// self.momentum * layer.bias_momentums
		bu1 = m_ncp.mult( layer->m_bias_momentums, m_momentum );
		// self.current_learning_rate * layer.dbiases
		bu2 = m_ncp.mult( layer->m_dbiases, m_current_learning_rate );
		// layer.bias_momentums = bias_updates
		layer->m_bias_momentums = m_ncp.subtract( bu1, bu2 );
		// layer.biases += bias_updates
		layer->m_biases = m_ncp.add( layer->m_biases, layer->m_bias_momentums );
		// debug
		// m_db.print( "layer->m_biases", layer->m_biases );
		
	} else
	{
		// Vanilla SGD updates
		vector<vector<double>> tmp;
		// -m_current_learning_rate * m_dweights
		for( const auto& vec : layer->m_dweights )
		{
			vector<double> tmp_vec;
			for( ulong i=0; i<vec.size(); i++ )
			{
				tmp_vec.push_back( -m_current_learning_rate * vec[i] );
			}
			tmp.push_back( tmp_vec );
		}
		// m_weights = m_weights +  tmp
		layer->m_weights = m_ncp.add( layer->m_weights, m_ncp.transpose( tmp ) );
		
		// layer.biases += -self.learning_rate * layer.dbiases
		for( ulong i=0; i<layer->m_biases.size(); i++ )
		{
			layer->m_biases[i] += -m_current_learning_rate * layer->m_dbiases[i];
		}
	}
}

void lsOptimizer_SGD::post_update_params()
{
	m_iterations++;
}


