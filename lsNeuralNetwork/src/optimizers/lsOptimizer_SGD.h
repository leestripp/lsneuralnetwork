#ifndef LSOPTIMIZER_SGD_H
#define LSOPTIMIZER_SGD_H

#include <iostream>

// numcpp
#include "numcpp.h"
// debug
#include "lsDebug.h"

// protos
class lsLayer_dense;

using namespace std;

class lsOptimizer_SGD
{
public:
	lsOptimizer_SGD( double learning_rate = 1.0, double decay=0.001, double momentum=0.9 );
	virtual ~lsOptimizer_SGD();
	
	void pre_update_params();
	void update_params( lsLayer_dense *layer );
	void post_update_params();
	
	// Params
	double m_learning_rate;
	double m_current_learning_rate;
	double m_decay;
	ulong m_iterations;
	double m_momentum;
	
	// numcpp
	numcpp m_ncp;
	// debug
	lsDebug m_db;
};

#endif // LSOPTIMIZER_SGD_H

