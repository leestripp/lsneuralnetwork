#ifndef LSDEBUG_H
#define LSDEBUG_H

#include <iostream>
#include <string>
#include <vector>

using namespace std;

class lsDebug
{
public:
	lsDebug();
	virtual ~lsDebug();
	
	void print( const string& msg );
	void print( const string& msg, const vector<ulong>& vec, ulong limit=10 );
	void print( const string& msg, const vector<double>& vec, ulong limit=10 );
	// Matrix
	void print( const string& msg, const vector<vector<double>>& mat, ulong limit=5 );
	void print( const string& msg, const vector<vector<ulong>>& mat, ulong limit=5 );
	
};

#endif // LSDEBUG_H
