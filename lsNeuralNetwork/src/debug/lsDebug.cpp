#include "lsDebug.h"

lsDebug::lsDebug()
{
}

lsDebug::~lsDebug()
{
}

void lsDebug::print( const string& msg )
{
	cout << msg << endl;
}

void lsDebug::print( const string& msg, const vector<ulong>& vec, ulong limit )
{
	if( vec.empty() ) return;
	
	cout << msg << " : size: " << vec.size() << endl;
	cout << "[ ";
	ulong i = 0;
	for( auto val : vec )
	{
		cout << val;
		if( i > limit )
		{
			cout << ", ...";
			break;
		}
		if( i < vec.size()-1 )
		{
			cout << ", ";
		}
		i++;
	}
	cout << " ]" << endl;
}

void lsDebug::print( const string& msg, const vector<double>& vec, ulong limit )
{
	if( vec.empty() )
	{
		cerr << "ERROR: lsDebug::print : vector empty." << endl;
		return;
	}
	
	cout << msg << " : size: " << vec.size() << endl;
	cout << "[ ";
	ulong i = 0;
	for( auto val : vec )
	{
		cout << val;
		if( i > limit )
		{
			cout << ", ...";
			break;
		}
		if( i < vec.size()-1 )
		{
			cout << ", ";
		}
		i++;
	}
	cout << " ]" << endl;
}

//**************
// Matrix

void lsDebug::print( const string& msg, const vector<vector<double>>& mat, ulong limit )
{
	if( mat.empty() )
	{
		cerr << "ERROR: lsDebug::print : " << msg << " - empty." << endl;
		return;
	}
	
	cout << msg << " : size: " << mat.size() << " x " << mat[0].size() << endl;
	ulong row = 0;
	
	cout << "[ ";
	for( const auto& vec : mat )
	{
		cout << "[ ";
		ulong i = 0;
		for( const auto& val : vec )
		{
			cout << val;
			if( i < vec.size()-1 )
			{
				cout << ", ";
			}
			i++;
		}
		
		if( row < mat.size()-1 && row < limit-1 )
		{
			cout << " ]" << endl;
		} else
		{
			cout << " ]";
			break;
		}
		row++;
	}
	// end cap
	if( mat.size() > limit )
	{
		cout << "\n..." << endl;
	} else
	{
		cout << " ]" << endl;
	}
}

void lsDebug::print( const string& msg, const vector<vector<ulong>>& mat, ulong limit )
{
	if( mat.empty() )
	{
		cerr << "ERROR: lsDebug::print : " << msg << " - empty." << endl;
		return;
	}
	
	cout << msg << " : size: " << mat.size() << " x " << mat[0].size() << endl;
	ulong row = 0;
	
	cout << "[ ";
	for( const auto& vec : mat )
	{
		cout << "[ ";
		ulong i = 0;
		for( const auto& val : vec )
		{
			cout << val;
			if( i < vec.size()-1 )
			{
				cout << ", ";
			}
			i++;
		}
		
		if( row < mat.size()-1 && row < limit-1 )
		{
			cout << " ]" << endl;
		} else
		{
			cout << " ]";
			break;
		}
		row++;
	}
	// end cap
	if( mat.size() > limit )
	{
		cout << "\n..." << endl;
	} else
	{
		cout << " ]" << endl;
	}
}
