#ifndef TRIENADE_H
#define TRIENADE_H

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <unordered_map>

using namespace std;

class trieNode
{
public:
	trieNode();
	
	// prev tokenID's
	void add_prev_tokenID( ulong ID );
	bool has_prev_tokenID( ulong ID );
	
	// tree
	unordered_map<char, trieNode*> node;
	// reverse lookup
	trieNode *m_parent;
	
	char m_ch;
	bool m_is_token;
	ulong m_tokenID;
	vector<ulong> m_prev_tokenID_list;
};

#endif // TRINADE_H