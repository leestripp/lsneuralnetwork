#ifndef LSCONTEXT_H
#define LSCONTEXT_H

#include <iostream>
#include <vector>
#include <tuple>
#include <iterator>
#include <numeric>
#include <algorithm>
#include <random>

using namespace std;

class lsContext
{
public:
	lsContext();
	
	// unique ID list.
	vector<ulong> gen_idlist();
	bool in_list( ulong buff_id );
	// Data
	tuple<vector<vector<double>>, vector<ulong>> gen_data( ulong num_tokens );
	vector<vector<double>> gen_predict_data( ulong pos, ulong num_tokens );
	vector<double> get_range( ulong pos, ulong num_tokens );
	// Random
	vector<double> gen_random( ulong MIN, ulong MAX, ulong NUMS );
	
	// debug
	void print();
	
	vector<ulong> m_list;
	vector<ulong> m_buffer;
};

#endif // LSCONTEXT_H
