#include "trieNode.h"

trieNode::trieNode()
{
	m_parent = NULL;
	m_ch = 0;
	m_is_token = false;
	m_tokenID = 0;
}

void trieNode::add_prev_tokenID( ulong ID )
{
	for( const ulong& val : m_prev_tokenID_list )
	{
		if( val == ID ) return;
	}
	m_prev_tokenID_list.push_back( ID );
}

bool trieNode::has_prev_tokenID( ulong ID )
{
	for( const ulong& val : m_prev_tokenID_list )
	{
		if( val == ID ) return true;
	}
	return false;
}
