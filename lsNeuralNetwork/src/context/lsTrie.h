#ifndef LSTRIE_H
#define LSTRIE_H

#include "trieNode.h"

using namespace std;

class lsTrie
{
public:
	lsTrie();
	
	void insert( const string& text );
	void clearTree();
	// find
	string find_token( ulong tokenID );
	trieNode* find_node( ulong tokenID );
	ulong find_tokenID( const string& token );
	trieNode* find_node( const string& token );
	// token lists
	vector<string>  iterate();
	vector<string> findPrefix( const string& prefix );
	ulong count();
	// debug
	void print();
	
private:
	void iterate_( const trieNode* node, const string& prefix );
	void clear_( const trieNode* trienode );
	void findPrefix_( const trieNode* node, const string& prefix, ulong pos );
	// debug
	void print_( const trieNode* node, const string& prefix );
	
	// store
	trieNode *root;
	ulong m_current_token;
	map<ulong, trieNode*> m_token_list;
	
	// find
	trieNode *m_found;
	vector<string> m_wordlist;
};

#endif // LSTRIE_H

