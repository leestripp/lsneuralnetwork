#include "lsTokenizer.h"

lsTokenizer::lsTokenizer()
{
	m_init_count = 0;
	m_max_prevIDs = 2;
	
	// default settings.
	set_split_by( "\" \t\r\n" );
	init();
}

void lsTokenizer::init()
{
	vector<string> defaults;
	
	// EOS and BOS
	defaults.push_back( "[PAD]" );
	defaults.push_back( "[BOS]" );
	defaults.push_back( "[EOS]" );
	m_init_count = defaults.size();
	
	// Add default tokens.
	insert_tokens( defaults, false );  // false = do not shuffle.
}

void lsTokenizer::clear()
{
	m_init_count = 0;
	m_trie.clearTree();
	// Add back defaults
	init();
}

// set_split_by

void lsTokenizer::set_split_by( const string& list )
{
	// split on
	for( const char& ch : list )
	{
		m_splitter_list.push_back( ch );
	}
}

// Load.

string lsTokenizer::load( const string& filename )
{
	stringstream ss;
	ifstream in( filename );
	if( in.is_open() )
	{
		ss << in.rdbuf();
	}
	return ss.str();
}

// Split into Tokens.

vector<string> lsTokenizer::tokenize( const string& data, bool lowercase )
{
	vector<string> tokens;
	string text = data;
	
	// convert to lowercase.
	if( lowercase )
	{
		transform( text.begin(), text.end(), text.begin(), [](unsigned char c){ return tolower(c); } );
	}
	
	stringstream ss;
	for( ulong i=0; i<text.size(); i++ )
	{
		if( is_splitter( text[i] ) )
		{
			// splitter char.
			if( ss.rdbuf()->in_avail() != 0 )
			{
				// add word.
				tokens.push_back( ss.str() );
				ss.str( string() );
			}
		} else
		{
			ss << text[i];
		}
	} // for i
	// grab last token.
	if( ss.rdbuf()->in_avail() != 0 )
	{
		tokens.push_back( ss.str() );
	}
	
	return tokens;
}

// Create Token IDs.

void lsTokenizer::insert_tokens( const vector<string>& tokens, bool do_shuffle )
{
	// Initialize random number generator
    random_device rd;
    mt19937 g(rd());
	
	// Shuffle the vector
	vector<string> shuffle_tokens = tokens;
	if( do_shuffle ) shuffle( shuffle_tokens.begin(), shuffle_tokens.end(), g );
	
	// token Tri-E
	for( const string& token : shuffle_tokens )
	{
		m_trie.insert( token );
	}
}

//******************
// find

string lsTokenizer::find_token( ulong tokenID )
{
	return m_trie.find_token( tokenID );
}

trieNode* lsTokenizer::find_node( ulong tokenID )
{
	return m_trie.find_node( tokenID );
}

ulong lsTokenizer::find_tokenID( const string& token )
{
	return m_trie.find_tokenID( token );
}

trieNode* lsTokenizer::find_node( const string& token )
{
	return m_trie.find_node( token );
}

//******************
// Convert

vector<ulong> lsTokenizer::toContext( const vector<string>& tokens, bool add_EOS )
{
	trieNode *node;
	vector<ulong> prev_IDs;
	vector<ulong> result;
	
	prev_IDs.push_back( 0 );		// [PAD]
	for( const string& token : tokens )
	{
		node = find_node( token );
		if( node )
		{
			for( const ulong& ID : prev_IDs )
			{
				node->add_prev_tokenID( ID );
			}
			result.push_back( node->m_tokenID );
			prev_IDs.push_back( node->m_tokenID );
			if( prev_IDs.size() > m_max_prevIDs )  prev_IDs.erase( prev_IDs.begin() );
		}
	}
	if( add_EOS ) result.push_back( find_tokenID( "[EOS]" ) );
	return result;
}

vector<string> lsTokenizer::convert( const vector<ulong>& ids )
{
	vector<string> result;
	for( ulong id : ids )
	{
		result.push_back( find_token( id ) );
	}
	return result;
}

vector<string> lsTokenizer::convert( const vector<double>& ids )
{
	vector<string> result;
	for( double id : ids )
	{
		result.push_back( find_token( (ulong)id ) );
	}
	return result;
}

//**************************
// Vocab : load and save

void lsTokenizer::load_vocab( const string& filename )
{
	cout << "Loading vocab : " << filename << endl;
	
	ifstream in( filename );
	if(! in.is_open() )
	{
		cerr << "ERROR: Unable to open vocab file : " << filename << endl;
		return;
	}
	
	vector<string> tokens;
	string line;
	while( getline( in, line ) )
	{
		// split line
		vector<string> result = split_line( line );
		if(! result.empty() )
		{
			// Add the token.
			tokens.push_back( result[0] );
		}
	}
	in.close();
	if( tokens.empty() ) return;
	// Add the tokens.
	insert_tokens( tokens, false );		// false = don't shuffle.
	
	// Add the previous IDs
	in.open( filename );
	if(! in.is_open() )
	{
		cerr << "ERROR: Unable to open vocab file : " << filename << endl;
		return;
	}
	// read lines again.
	while( getline( in, line ) )
	{
		// split line
		vector<string> result = split_line( line );
		if(! result.empty() )
		{
			trieNode *node = find_node( result[0] );
			if( result.size() > 1 && node )
			{
				for( ulong k=1; k<result.size(); k++ )
				{
					node->m_prev_tokenID_list.push_back( stoul(result[k]) );
				}
			}
		}
	}
	in.close();
}

void lsTokenizer::save_vocab( const string& filename )
{
	cout << "Saving vocab : " << filename << endl;
	
	ofstream out( filename );
	if(! out.is_open() )
	{
		cerr << "ERROR: Unable to open vocab file : " << filename << endl;
		return;
	}
	
	// skip first token defaults.
	for( ulong i=m_init_count; i < token_count(); i++  )
	{
		// Get the token text
		string token = find_token( i );
		out << token;
		
		trieNode *node = find_node( i );
		if( node )
		{
			if(! node->m_prev_tokenID_list.empty() )
			{
				// Add space after token.
				out << " ";
				
				for( ulong i=0; i<node->m_prev_tokenID_list.size(); i++ )
				{
					out << node->m_prev_tokenID_list[i];
					if( i < node->m_prev_tokenID_list.size()-1 ) out << " ";
				}
			}
		}
		out << endl;
	}
	out.close();
}

//**************************
// Utils

bool lsTokenizer::is_splitter( const char& ch )
{
	for( const char& c : m_splitter_list )
	{
		if( ch == c ) return true;
	}
	return false;
}

ulong lsTokenizer::token_count()
{
	return m_trie.count();
}

vector<vector<string>> lsTokenizer::split_tokens( const vector<string>& tokens, ulong block_size )
{
	vector<vector<string>> token_blocks;
	vector<string> tmp;
	ulong count = 0;
	
	for( const string& token : tokens )
	{
		tmp.push_back( token );
		count++;
		
		if( count > block_size )
		{
			token_blocks.push_back( tmp );
			tmp.clear();
			count = 0;
		}
	}
	// catch leftovers or small blocks.
	if(! tmp.empty() )
	{
		token_blocks.push_back( tmp );
	}
	
	return token_blocks;
}

vector<string> lsTokenizer::split_line( const string& line )
{
	vector<string> result;
	
	stringstream ss;
	for( ulong i=0; i<line.size(); i++ )
	{
		if( line[i] == ' ' )
		{
			// check stream
			if( ss.rdbuf()->in_avail() != 0 )
			{
				// add item.
				result.push_back( ss.str() );
				ss.str( string() );
			}
		} else
		{
			ss << line[i];
		}
	} // for i
	// grab last token.
	if( ss.rdbuf()->in_avail() != 0 )
	{
		result.push_back( ss.str() );
	}
	
	return result;
}

//****************
// debug

void lsTokenizer::print()
{
	m_trie.print();
	cout << "Token count : " << token_count() << endl;
}
