#include <filesystem>

#include "lsTrie.h"

lsTrie::lsTrie()
{
	m_current_token = 0;
	root = new trieNode();
	if(! root )
	{
		cerr << "ERROR: lsTrie : Unable to create root node." << endl;
	}
}

void lsTrie::insert( const string& text )
{
	if( text.empty() ) return;
	if(! root ) return;
	
	trieNode *tmp = root;
	for( ulong i=0; i<text.length(); i++ )
	{
		if( tmp->node.find( text[i] ) == tmp->node.end() )
		{
			// No letter found, so store it and add new Node.
			trieNode *child = new trieNode();
			child->m_ch = text[i];
			child->m_parent = tmp;
			tmp->node[ text[i] ] = child;
		}
		// next node.
		tmp = tmp->node[ text[i] ];
    }
	
    // whole token added or exists.
	if( tmp->m_is_token )
	{
		// Allready exists.
		// debug
		// cout << "Add token: " << text << ", token ID: " << tmp->m_tokenID;
		// cout << " *** ESISTS"<< endl;
		
	} else
	{
		// debug
		// cout << "Add token: " << text << ", ID: " << m_current_token << endl;
		
		tmp->m_is_token = true;
		tmp->m_tokenID = m_current_token;
		m_token_list[ m_current_token ] = tmp;
		m_current_token++;
	}
}

// reverse lookup of trie.

string lsTrie::find_token( ulong tokenID )
{
	if( m_token_list.empty() ) return string();
	if( m_token_list.size() < tokenID+1 ) return string();
	
	vector<char> text;
	trieNode *trienode;
	
	trienode = m_token_list[ tokenID ];
	if(! trienode ) return string();
	
	text.insert( text.begin(), trienode->m_ch );
	while( (trienode = trienode->m_parent) )
	{
		if( trienode->m_parent != NULL ) text.insert( text.begin(), trienode->m_ch );
	}
	
	return string( text.begin(), text.end() );
}

trieNode* lsTrie::find_node( ulong tokenID )
{
	if( m_token_list.empty() ) return NULL;
	if( m_token_list.size() < tokenID+1 ) return NULL;
	
	return m_token_list[ tokenID ];
}

ulong lsTrie::find_tokenID( const string& token )
{
	ulong pos = 0;
	m_found = NULL;
	
	if( root )
	{
		// debug
		// cout << "Find token : " << token << endl;
		findPrefix_( root, token, pos );
		
		// token found.
		if( m_found )
		{
			return m_found->m_tokenID;
		}
	}
	
	return 0;
}

trieNode* lsTrie::find_node( const string& token )
{
	ulong pos = 0;
	m_found = NULL;
	
	if( root )
	{
		// debug
		// cout << "Find token : " << token << endl;
		findPrefix_( root, token, pos );
		
		// token found.
		if( m_found )
		{
			return m_found;
		}
	}
	return NULL;
}


void lsTrie::iterate_( const trieNode* trienode, const std::string& prefix )
{
	if( trienode->m_is_token )
	{
		m_wordlist.push_back( prefix );
	}
	
	for( const auto& [c, child] : trienode->node )
	{
		iterate_( child, prefix + c );
	}
}

vector<string> lsTrie::iterate()
{
	m_wordlist.clear();
	
	if( root )
	{
		iterate_( root, "" );
	} else
	{
		cout << "Tree empty..." << endl;
	}
	
	return m_wordlist;
}

void lsTrie::findPrefix_( const trieNode* node, const string& prefix, ulong pos )
{
	trieNode *tmp;
	
	if( auto search = node->node.find( prefix[pos] ); search != node->node.end() )
	{
		tmp = search->second;
		
		if( prefix.size() == pos+1 )
		{
			m_found = tmp;
			return;
		} else
		{
			pos++;
			findPrefix_( tmp, prefix, pos );
		}
	}
}

vector<string> lsTrie::findPrefix( const string& prefix )
{
	m_found = NULL;
	ulong pos = 0;
	
	if( root )
	{
		// debug
		// cout << "Find tokens with prefix: " << prefix << endl;
		findPrefix_( root, prefix, pos );
		
		// display tokens from node found.
		if( m_found )
		{
			// clear word list
			m_wordlist.clear();
			// Add new words
			iterate_( m_found, prefix );
		}
	} else
	{
		// Clear word list
		m_wordlist.clear();
	}
	
	return m_wordlist;
}

ulong lsTrie::count()
{
	return m_token_list.size();
}


void lsTrie::clear_( const trieNode* trienode )
{
	for( const auto& [c, child] : trienode->node )
	{
		// debug
		// cout << " " << c;
		clear_( child );
		delete child;
	}
}

void lsTrie::clearTree()
{
	// Clear token list.
	m_token_list.clear();
	
	// clear tree
	if( root )
	{
		// debug
		// cout << "clearNodes:";
		clear_( root );
		delete root;
		// cout << endl;
	}
	
	// recreate our root node.
	root = new trieNode();
	if(! root )
	{
		cerr << "ERROR: lsTrie : Unable to create root node." << endl;
	}
	m_current_token = 0;
}

//*****************
// debug

void lsTrie::print_( const trieNode* trienode, const std::string& prefix )
{
	if( trienode->m_is_token )
	{
		cout << "Token: '" << prefix;
		cout << "', token ID: " << trienode->m_tokenID << endl;
	}
	
	for( const auto& [c, child] : trienode->node )
	{
		print_( child, prefix + c );
	}
}

void lsTrie::print()
{
	if( root )
	{
		// debug
		cout << "\nPrint all Token texts and IDs." << endl;
		
		print_( root, "" );
	} else
	{
		cout << "Tree empty..." << endl;
	}
}

