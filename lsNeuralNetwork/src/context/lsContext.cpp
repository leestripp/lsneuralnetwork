#include "lsContext.h"

lsContext::lsContext()
{
}

// generate a unique list of IDs in the context.

vector<ulong> lsContext::gen_idlist()
{
	m_list.clear();
	for( ulong pos=0; pos<m_buffer.size(); pos++ )
	{
		if(! in_list( m_buffer[pos] ) )
		{
			m_list.push_back( m_buffer[pos] );
		}
	}
	return m_list;
}

bool lsContext::in_list( ulong buff_id )
{
	for( const ulong& id : m_list )
	{
		if( id == buff_id ) return true;
	}
	return false;
}

//***************************
// Data

tuple<vector<vector<double>>, vector<ulong>> lsContext::gen_data( ulong num_tokens )
{
	if( num_tokens < 2 ) num_tokens = 2;
	
	// Create dataset from context, based on tokenID
	vector<vector<double>> X;
	vector<ulong> y;
	
	// step though context and add data.
	for( ulong pos=1; pos<m_buffer.size(); pos++ )
	{
		auto data = get_range( pos, num_tokens+1 );		// +1 = get learn tokenID as well.
		if( data.empty() ) continue;
		
		// Get learn tokenID
		ulong learn_tokenID = data[num_tokens];
		
		// Remove the last tokenID, its the one we want to learn.
		data.erase( data.end() );
		
		// the one we want to learn.
		X.push_back( data );
		y.push_back( learn_tokenID );
		
	} // for pos
	
    return make_tuple( X, y );
}

vector<vector<double>> lsContext::gen_predict_data( ulong pos, ulong num_tokens )
{
	if( num_tokens < 2 ) num_tokens = 2;
	
	vector<vector<double>> X;
	
	auto data = get_range( pos, num_tokens );
	if(! data.empty()  )
	{
		X.push_back( data );
	}
	return X;
}

vector<double> lsContext::get_range( ulong pos, ulong num_tokens )
{
	vector<double> data_main;
	
	// get tokens before pos.
	ulong offset = num_tokens-1;
	for( ulong i=0; i<num_tokens; i++ )
	{
		if( (pos+i) < offset )
		{
			data_main.push_back( 0 );		// [PAD]
		} else
		{
			if( (pos + i) - offset < m_buffer.size() )
			{
				data_main.push_back( m_buffer[ (pos+i) - offset ] );
			} else
			{
				data_main.push_back( 2 );	// [EOS]
			}
		}
	}
	
	return data_main;
}

vector<double> lsContext::gen_random( ulong MIN, ulong MAX, ulong NUMS )
{
	vector<double> X;
	
	random_device rd;
	mt19937 g( rd() );
	std::uniform_int_distribution<> dis( MIN, MAX );
	
	for( ulong i=0; i<NUMS; i++ )
	{
		X.push_back( dis(g) );
	}
	return X;
}


//***********************
// Debug

void lsContext::print()
{
	ulong c=1;
	cout << "Context [ ";
	for( ulong id : m_buffer )
	{
		cout << id;
		if( c < m_buffer.size() ) cout << ", ";
		c++;
	}
	cout << " ]" << endl;
}
