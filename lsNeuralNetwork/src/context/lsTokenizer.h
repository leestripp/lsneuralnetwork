#ifndef LSTOKENIZER_H
#define LSTOKENIZER_H

#include <iostream>
#include <sstream>
#include <filesystem>
#include <fstream>
#include <vector>
#include <algorithm>
#include <random>

// Trie
#include "lsTrie.h"

using namespace std;

class lsTokenizer
{
public:
	lsTokenizer();
	
	void set_split_by( const string& list );
	vector<string> tokenize( const string& data, bool lowercase );
	void insert_tokens( const vector<string>& tokens, bool do_shuffle=true );
	void clear();
	
	// convert to and from lsContext
	vector<ulong> toContext( const vector<string>& tokens, bool add_EOS=false );
	vector<string> convert( const vector<ulong>& ids );
	vector<string> convert( const vector<double>& ids );
	
	// find
	string find_token( ulong tokenID );
	trieNode* find_node( ulong tokenID );
	ulong find_tokenID( const string& token );
	trieNode* find_node( const string& token );
	
	// Vocab : load and save
	void load_vocab( const string& filename );
	void save_vocab( const string& filename );
	
	// Utils
	// load text file.
	string load( const string& filename );
	ulong token_count();
	vector<vector<string>> split_tokens( const vector<string>& tokens, ulong block_size );
	vector<string> split_line( const string& line );
	// debug
	void print();
	
	// get set
	void set_max_prevIDs( ulong val )
	{
		m_max_prevIDs = val;
	}
	
private:
	void init();
	bool is_splitter( const char& ch );
	
	// items to split on
	vector<char> m_splitter_list;
	// default token count.
	ulong m_init_count;
	ulong m_max_prevIDs;
	
	lsTrie m_trie;
};

#endif // LSTOKENIZER_H
