#ifndef LSLOSS_CCE_H
#define LSLOSS_CCE_H

#include <iostream>
#include <vector>

// numcpp
#include "numcpp.h"
// debug
#include "lsDebug.h"

using namespace std;

// Categorical Cross-Entropy Loss

class lsLoss_CCE
{
public:
	lsLoss_CCE();
	virtual ~lsLoss_CCE();
	
	double calculate( const vector<vector<double>>& inputs, const vector<ulong>& y );
	
	void forward( const vector<vector<double>>& inputs, const vector<ulong>& y );
	void backward( const vector<vector<double>>& dvalues, const vector<ulong>& y );
	
	// Forward pass
	// output
	vector<double> m_losses;
	
	// Backward pass
	vector<vector<double>> m_dinputs;
	
	// numcpp
	numcpp m_ncp;
	// debug
	lsDebug m_db;
};

#endif // LSLOSS_CCE_H