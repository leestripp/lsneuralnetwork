#include "lsLoss_CCE.h"

lsLoss_CCE::lsLoss_CCE()
{

}

lsLoss_CCE::~lsLoss_CCE()
{
}

//**********************
// Forward pass

double lsLoss_CCE::calculate( const vector<vector<double>>& inputs, const vector<ulong>& y )
{
	// cleanup last pass
	m_losses.clear();
	
	// y should be correct.
	forward( inputs, y );
	
	// debug
	// m_db.print( "m_losses", m_losses );
	
	return m_ncp.mean( m_losses );
}

void lsLoss_CCE::forward( const vector<vector<double>>& inputs, const vector<ulong>& y )
{
	// debug
	// cout << "lsLoss_CCE::forward" << endl;
	
	vector<double> correct_confidences;
	
	// debug
	// m_db.print( "inputs", inputs );
	
	// samples
	ulong i=0;
	for( const auto& sample : inputs )
	{
		// debug
		// m_db.print( "sample", sample );
		// cout << "sample y true clipped : " << m_ncp.clip( sample[ y[i] ], 1.0e-07, 1.0-1.0e-07 ) << endl;
		
		correct_confidences.push_back( m_ncp.clip( sample[ y[i] ], 1.0e-07, 1.0-1.0e-07 ) );
		i++;
	}
	// debug
	// m_db.print( "correct_confidences", correct_confidences );
	
	// negative_log_likelihoods
	for( ulong i=0; i<inputs.size(); i++ )
	{
		m_losses.push_back( -log( correct_confidences[i] ) );
	}
	// debug
	// m_db.print( "m_losses", m_losses );
}

//**********************
// Backward pass

void lsLoss_CCE::backward( const vector<vector<double>>& dvalues, const vector<ulong>& y )
{
	// debug
	// cout << "lsLoss_CCE::backward" << endl;
	
	// cleanup last pass
	m_dinputs.clear();
	
	// convert y to One-Hot
	vector<vector<ulong>> y_true = m_ncp.sparse2onehot( y, dvalues[0].size() );
	// debug
	// m_db.print( "dvalues", dvalues );
	// m_db.print( "y_true", y_true );
	
	// Number of samples
	ulong samples = dvalues.size();
	
	// Calculate gradient
	// m_dinputs = -y_true / dvalues
	vector<vector<double>> tmp_dinputs;
	for( ulong i=0; i<dvalues.size(); i++ )
	{
		vector<double> tmp;
		for( ulong k=0; k<dvalues[0].size(); k++ )
		{
			// debug
			// cout << "-y_true / dvalue : " << -double( y_true[i][k] ) << " / " << dvalues[i][k] << endl;
			
			tmp.push_back( -double( y_true[i][k] ) / dvalues[i][k] );
		}
		tmp_dinputs.push_back( tmp );
	}
	// debug
	// m_db.print( "tmp_dinputs", tmp_dinputs );
	
	// Normalize gradient
	// m_dinputs = m_dinputs / samples
	for( ulong i=0; i<tmp_dinputs.size(); i++ )
	{
		vector<double> tmp;
		for( ulong k=0; k<tmp_dinputs[0].size(); k++ )
		{
			tmp.push_back( tmp_dinputs[i][k] / samples );
		}
		m_dinputs.push_back( tmp );
	}
	// debug
	// m_db.print( "m_dinputs", m_dinputs );
}

