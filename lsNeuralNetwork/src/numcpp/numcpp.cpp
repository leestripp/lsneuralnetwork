#include "numcpp.h"

// Threads
#include "numcpp_thread.h"

numcpp::numcpp()
{
    // Threads
    m_thread_count = thread::hardware_concurrency();
	m_block_size = 50;
	
	// Random
	m_rng = new default_random_engine( m_rd() );
}

numcpp::~numcpp()
{
	if( m_rng ) delete m_rng;
}

//***************************************
// Vector

double numcpp::add( const vector<double>& vec )
{
	double res = 0.0;
	for( const auto& val : vec )
	{
		res += val;
	}
	return res;
}

vector<double> numcpp::add( const vector<double>& vec1, const vector<double>& vec2 )
{
	vector<double> res( vec1.size() );
	for( ulong i=0; i<vec1.size(); i++ )
	{
		res[i] = vec1[i] + vec2[i];
	}
	return res;
}

vector<double> numcpp::subtract( const vector<double>& vec1, const vector<double>& vec2 )
{
	vector<double> res( vec1.size() );
	for( ulong i=0; i<vec1.size(); i++ )
	{
		res[i] = vec1[i] - vec2[i];
	}
	return res;
}

vector<double> numcpp::mult( const vector<double>& vec, double val )
{
	vector<double> res( vec.size() );
	for( ulong i=0; i<vec.size(); i++ )
	{
		res[i] = vec[i] * val;
	}
	return res;
}

// vector dot product
double numcpp::dot( const vector<double>& vec1, const vector<double>& vec2 )
{
	double res = 0.0;
	for( ulong i=0; i<vec1.size(); i++ )
	{
		res += vec1[i] * vec2[i];
	}
	return res;
}

//***************************************
// Matrix

vector<vector<double>> numcpp::normalise_m1_1( const vector<vector<double>>& mat, bool use_min )
{
	vector<vector<double>> res;
	
	// find min and max.
	tuple[ t_min, t_max ] = find_min_max( mat[0] );
	for( const auto& vec : mat )
	{
		tuple[ min, max ] = find_min_max( vec );
		if( min < t_min ) t_min = min;
		if( max > t_max ) t_max = max;
	}
	if(! use_min  ) t_min = 0.0;
	
	// normalise. -1 : 1
	for( const auto& vec : mat )
	{
		vector<double> tmp;
		for( double val : vec )
		{
			val = (val - t_min) / (t_max - t_min);
			val *= 2.0;
			val -= 1.0;
			tmp.push_back( val );
		}
		res.push_back( tmp );
	}
	return res;
}


// add matrix rows.
vector<double> numcpp::add( const vector<vector<double>>& mat, int axis )
{
	vector<double> res;
	
	switch( axis )
	{
		case NCP_AXIS_ALL:
		{
			double k=0.0;
			for( const auto& vec : mat )
			{
				k += add( vec );
			}
			res.push_back( k );
			break;
		}
		
		case NCP_AXIS_ROW:
		{
			for( const auto& vec : mat )
			{
				res.push_back( add( vec ) );
			}
			break;
		}
		
		case NCP_AXIS_COL:
		{
			for( ulong mc=0; mc<mat[0].size(); mc++ )
			{
				double k=0.0;
				for( ulong mr=0; mr<mat.size(); mr++ )
				{
					k += mat[mr][mc];
				}
				res.push_back( k );
			}
			break;
		}
	}
	return res;
}


// matrix + vector - adding biases

vector<vector<double>> numcpp::add( const vector<vector<double>>& mat, const vector<double>& vec )
{
	vector<vector<double>> res;
	
	// Check shape
	if( mat[0].size() != vec.size() )
	{
		cerr << "ERROR: numcpp::add : matrix to vector shape missmatch." << endl;
		cerr << "matrix shape mat: " << mat.size() << "x" << mat[0].size();
		cerr << ", vec: " << vec.size() << endl;
		return res;
	}
	
	// Thread it
	if( mat.size() > m_block_size )
	{
		// cout << "TODO: numcpp::add : Thread it" << endl;
		return add_threaded( mat, vec );
	}
	
	// rows
	for( const auto& vec2 : mat )
	{
		vector<double> tmp( vec.size() );
		for( ulong i=0; i<vec.size(); i++ )
		{
			tmp[i] = vec2[i] + vec[i];
		}
		res.push_back( tmp );
	}
	return res;
}

vector<vector<double>> numcpp::add_threaded( const vector<vector<double>>& mat, const vector<double>& vec )
{
	vector<vector<double>> res;
	
	// split data
	vector<vector<vector<double>>> blocks_mat;
	vector<vector<double>> tmp_mat;
	ulong rc = 0;
	for( ulong i=0; i<mat.size(); i++ )
	{
		tmp_mat.push_back( mat[i] );
		rc++;
		
		if( rc >= m_block_size )
		{
			blocks_mat.push_back( tmp_mat );
			rc = 0;
			tmp_mat.clear();
		}
	}
	// Grab last block.
	if(! tmp_mat.empty() )
	{
		blocks_mat.push_back( tmp_mat );
	}
	// debug
	// m_db.print( "blocks_mat[0]", blocks_mat[0] );
	// m_db.print( "vec", vec );
	
	// run threads for each block.
	vector<thread> th;
	ulong tc = 0;
	for( ulong i=0; i<blocks_mat.size(); i++ )
	{
		// new thread class.
		numcpp_thread *ncp_th = new numcpp_thread;
		
		// set data
		ncp_th->m_mat1 = blocks_mat[i];
		ncp_th->m_vec1 = vec;
		m_ncp_threads.push_back( ncp_th );
		
		// Add thread
		th.push_back( thread( &numcpp_thread::add_mv, ncp_th ) );
		tc++;
		
		if( tc >= m_thread_count )
		{
			// Join and wait
			for( auto& t : th )
			{
				t.join();
			}
			th.clear();
			tc = 0;
		}
	}
	// Catch last threads.
	if(! th.empty() )
	{
		// Join and wait
		for( auto& t : th )
		{
			t.join();
		}
	}
	
	// collect data
	for( const auto& ncp_t : m_ncp_threads )
	{
		for( const auto& row : ncp_t->m_res_mat )
		{
			res.push_back( row );
		}
	}
	// debug
	// m_db.print( "result", res );
	
	// cleanup
	// delete instances.
	for( ulong i=0; i<m_ncp_threads.size(); i++ )
	{
		delete m_ncp_threads[i];
	}
	m_ncp_threads.clear();
	
	return res;
}


// matrix + matrix

vector<vector<double>> numcpp::add( const vector<vector<double>>& mat1, const vector<vector<double>>& mat2 )
{
	vector<vector<double>> res;
	
	// Check shape
	if( mat1.size() != mat2.size() || mat1[0].size() != mat2[0].size() )
	{
		cerr << "ERROR: numcpp::add : matrix shape missmatch." << endl;
		cerr << "matrix shape mat1: " << mat1.size() << "x" << mat1[0].size();
		cerr << ", mat2: " << mat2.size() << "x" << mat2[0].size() << endl;
		return res;
	}
	
	// rows
	for( ulong i=0; i<mat1.size(); i++ )
	{
		res.push_back( add( mat1[i], mat2[i] ) );
	}
	return res;
}

// matrix - matrix

vector<vector<double>> numcpp::subtract( const vector<vector<double>>& mat1, const vector<vector<double>>& mat2 )
{
	vector<vector<double>> res;
	
	// Check shape
	if( mat1.size() != mat2.size() || mat1[0].size() != mat2[0].size() )
	{
		cerr << "ERROR: numcpp::subtract : matrix shape missmatch." << endl;
		cerr << "matrix shape mat1: " << mat1.size() << "x" << mat1[0].size();
		cerr << ", mat2: " << mat2.size() << "x" << mat2[0].size() << endl;
		return res;
	}
	
	// Thread it
	if( mat1.size() > m_block_size || mat2.size() > m_block_size )
	{
		// cout << "TODO: numcpp::subtract matrix - matrix : Thread it" << endl;
		return subtract_threaded( mat1, mat2 );
	}
	
	for( ulong i=0; i<mat1.size(); i++ )
	{
		vector<double> tmp( mat1[0].size() );
		for( ulong k=0; k<mat1[0].size(); k++ )
		{
			tmp[k] = mat1[i][k] - mat2[i][k];
		}
		res.push_back( tmp );
	}
	return res;
}

vector<vector<double>> numcpp::subtract_threaded( const vector<vector<double>>& mat1, const vector<vector<double>>& mat2 )
{
	vector<vector<double>> res;
	
	// split data
	vector<vector<vector<double>>> blocks_mat1;
	vector<vector<vector<double>>> blocks_mat2;
	vector<vector<double>> tmp_mat1;
	vector<vector<double>> tmp_mat2;
	ulong rc = 0;
	for( ulong i=0; i<mat1.size(); i++ )
	{
		tmp_mat1.push_back( mat1[i] );
		tmp_mat2.push_back( mat2[i] );
		rc++;
		
		if( rc >= m_block_size )
		{
			blocks_mat1.push_back( tmp_mat1 );
			blocks_mat2.push_back( tmp_mat2 );
			rc = 0;
			tmp_mat1.clear();
			tmp_mat2.clear();
		}
	}
	// Grab last block.
	if(! tmp_mat1.empty() )
	{
		blocks_mat1.push_back( tmp_mat1 );
		blocks_mat2.push_back( tmp_mat2 );
	}
	
	// run threads for each block.
	vector<thread> th;
	ulong tc = 0;
	for( ulong i=0; i<blocks_mat1.size(); i++ )
	{
		// new thread class.
		numcpp_thread *ncp_th = new numcpp_thread;
		
		// set data
		ncp_th->m_mat1 = blocks_mat1[i];
		ncp_th->m_mat2 = blocks_mat2[i];
		m_ncp_threads.push_back( ncp_th );
		
		// Add thread
		th.push_back( thread( &numcpp_thread::subtract_mm, ncp_th ) );
		tc++;
		
		if( tc >= m_thread_count )
		{
			// Join and wait
			for( auto& t : th )
			{
				t.join();
			}
			th.clear();
			tc = 0;
		}
	}
	// Catch last threads.
	if(! th.empty() )
	{
		// Join and wait
		for( auto& t : th )
		{
			t.join();
		}
	}
	
	// collect data
	for( const auto& ncp_t : m_ncp_threads )
	{
		for( const auto& row : ncp_t->m_res_mat )
		{
			res.push_back( row );
		}
	}
	
	// cleanup
	// delete instances.
	for( ulong i=0; i<m_ncp_threads.size(); i++ )
	{
		delete m_ncp_threads[i];
	}
	m_ncp_threads.clear();
	
	return res;
}


// matrix from 2 vectors.
vector<vector<double>> numcpp::mult( const vector<double>& vec1, const vector<double>& vec2 )
{
	vector<vector<double>> res;
	for( const auto& val1 : vec1 )
	{
		vector<double> tmp( vec2.size() );
		for( ulong i=0; i<vec2.size(); i++ )
		{
			tmp[i] = val1 * vec2[i];
		}
		res.push_back( tmp );
	}
	return res;
}

// matrix * double

vector<vector<double>> numcpp::mult( const vector<vector<double>>& mat, double val )
{
	vector<vector<double>> res;
	for( ulong i=0; i<mat.size(); i++ )
	{
		vector<double> tmp( mat[0].size() );
		for( ulong k=0; k<mat[0].size(); k++ )
		{
			tmp[k] = mat[i][k] * val;
		}
		res.push_back( tmp );
	}
	return res;
}

// matrix * matrix : row * row

vector<vector<double>> numcpp::mult( const vector<vector<double>>& mat1, const vector<vector<double>>& mat2 )
{
	vector<vector<double>> res;
	
	// check shape.
	if( mat1.size() != mat2.size() || mat1[0].size() != mat2[0].size() )
	{
		cerr << "ERROR: numcpp::mult : matrix shape missmatch." << endl;
		cerr << "matrix shape mat1: " << mat1.size() << "x" << mat1[0].size();
		cerr << ", mat2: " << mat2.size() << "x" << mat2[0].size() << endl;
		return res;
	}
	
	
	// rows
	for( ulong i=0; i<mat1.size(); i++ )
	{
		vector<double> tmp( mat1[0].size() );
		for( ulong k=0; k<mat1[0].size(); k++ )
		{
			tmp[k] = mat1[i][k] * mat2[i][k];
		}
		res.push_back( tmp );
	}
	return res;
}


// row . col : matrix . matrix

vector<vector<double>> numcpp::dot( const vector<vector<double>>& mat1, const vector<vector<double>>& mat2 )
{
	// debug
	// cout << "numcpp::dot : Called..." << endl;
	
	vector<vector<double>> res;
	
	// check shape.
	if( mat1[0].size() != mat2.size() )
	{
		cerr << "ERROR: numcpp::dot : matrix shape missmatch. match row to col." << endl;
		cerr << "matrix shape mat1: " << mat1.size() << "x" << mat1[0].size();
		cerr << ", mat2: " << mat2.size() << "x" << mat2[0].size() << endl;
		return res;
	}
	
	// Thread it
	if( mat1.size() > m_block_size )
	{
		return dot_threaded( mat1, mat2 );
	}
	
	// iterate rows of mat1
	for( const auto& row : mat1 )
	{
		vector<double> tmp_row;
		// for each col of mat2
		for( ulong m2c=0; m2c<mat2[0].size(); m2c++ )
		{
			double val=0.0;
			for( ulong k=0; k<row.size(); k++ )
			{
				val += row[k] * mat2[k][m2c];
			}
			tmp_row.push_back( val );
		}
		res.push_back( tmp_row );
	}
	// debug
	// m_db.print( "result", res );
	
	return res;
}

vector<vector<double>> numcpp::dot_threaded( const vector<vector<double>>& mat1, const vector<vector<double>>& mat2 )
{
	vector<vector<double>> res;
	
	// split data
	vector<vector<vector<double>>> blocks;
	vector<vector<double>> tmp;
	ulong rc = 0;
	for( ulong i=0; i<mat1.size(); i++ )
	{
		tmp.push_back( mat1[i] );
		rc++;
		
		if( rc >= m_block_size )
		{
			blocks.push_back( tmp );
			rc = 0;
			tmp.clear();
		}
	}
	// Grab last block.
	if(! tmp.empty() )
	{
		blocks.push_back( tmp );
	}
	
	// run threads for each block.
	vector<thread> th;
	ulong tc = 0;
	for( const auto& block : blocks )
	{
		// new thread class.
		numcpp_thread *ncp_th = new numcpp_thread;
		
		// set data
		ncp_th->m_mat1 = block;
		ncp_th->m_mat2 = mat2;
		m_ncp_threads.push_back( ncp_th );
		
		// Add thread
		th.push_back( thread( &numcpp_thread::dot_mm, ncp_th ) );
		tc++;
		
		if( tc >= m_thread_count )
		{
			// Join and wait
			for( auto& t : th )
			{
				t.join();
			}
			th.clear();
			tc = 0;
		}
	}
	// Catch last threads.
	if(! th.empty() )
	{
		// Join and wait
		for( auto& t : th )
		{
			t.join();
		}
	}
	
	// collect data
	for( const auto& ncp_t : m_ncp_threads )
	{
		for( const auto& row : ncp_t->m_res_mat )
		{
			res.push_back( row );
		}
	}
	
	// cleanup
	// delete instances.
	for( ulong i=0; i<m_ncp_threads.size(); i++ )
	{
		delete m_ncp_threads[i];
	}
	m_ncp_threads.clear();
	
	return res;
}


// Matrix . vector

vector<double> numcpp::dot( const vector<vector<double>>& mat, const vector<double>& vec )
{
	vector<double> res;
	
	for( ulong i=0; i<mat.size(); i++ )
	{
		double val = 0.0;
		for( ulong k=0; k<vec.size(); k++ )
		{
			val += mat[i][k] * vec[k];
		}
		res.push_back( val );
	}
	return res;
}

// matrix transpose
vector<vector<double>> numcpp::transpose( const vector<vector<double>>& mat )
{
	vector<vector<double>> res;
    for( ulong i=0; i<mat[0].size(); i++ )
	{
        res.push_back({});
        for( ulong j=0; j<mat.size(); j++ )
		{
            res[i].push_back( mat[j][i] );
        }
    }
	return res;
}


//***********************
// Utilities

vector<double> numcpp::normalise_0_1( const vector<double>& vec )
{
	vector<double> res;
	
	tuple [ min, max ] = find_min_max( vec );
	for( const auto& val : vec )
	{
		res.push_back( (val-min) / (max-min) );
	}
	return res;
}

vector<double> numcpp::normalise_m1_1( const vector<double>& vec )
{
	vector<double> res;
	
	tuple [ min, max ] = find_min_max( vec );
	for( const auto& val : vec )
	{
		double val2 = (val-min) / (max-min);		//  0 - 1
		val2 = (val2 * 2.0) -1.0;					// -1 - 1
		res.push_back( val2 );
	}
	return res;
}

// clip
double numcpp::clip( double n, double lower, double upper )
{
  return std::max( lower, std::min( n, upper ) );
}

// for class targets

ulong numcpp::argmax( const vector<ulong>& vec )
{
	ulong res = 0;
	ulong m = find_max( vec );
	for( ulong i=0; i<vec.size(); i++ )
	{
		if( vec[i] == m )
		{
			return i;
		}
	}
	return res;
}

ulong numcpp::argmax( const vector<double>& vec )
{
	ulong res = 0;
	double m = find_max( vec );
	for( ulong i=0; i<vec.size(); i++ )
	{
		if( vec[i] == m )
		{
			return i;
		}
	}
	return res;
}

// normalise vector
vector<double> numcpp::normalise( const vector<double>& vec )
{
	vector<double> res;
	
	double s = add( vec );
	for( const auto& val : vec )
	{
		res.push_back( val / s );
	}
	return res;
}

// tuple [ min, max ] = find_min_max( vec );
tuple<double, double> numcpp::find_min_max( const vector<double>& vec )
{
	double min = -1.0;
	double max = 1.0;
	if( vec.empty() )
	{
		cerr << "ERROR: numcpp::find_min_max : vector empty." << endl;
		return make_tuple( min, max );
	}
	
	min = vec[0];
	max = min;
	for( double val : vec )
	{
		if( val > max ) max = val;
		if( val < min ) min = val;
	}
	return make_tuple( min, max );
}


// double m = m_ncp.find_max( vec );

double numcpp::find_max( const vector<double>& vec )
{
	double res = vec[0];
	for( const double& val : vec )
	{
		if( val > res ) res = val;
	}
	return res;
}

ulong numcpp::find_max( const vector<ulong>& vec )
{
	ulong res = vec[0];
	for( ulong val : vec )
	{
		if( val > res ) res = val;
	}
	return res;
}

// m_ncp.max( vec, 0.0 )
vector<double> numcpp::max( const vector<double>& vec, double m )
{
	vector<double> res;
	for( const auto& val : vec )
	{
		if( val > m )
			res.push_back( val );
		else
			res.push_back( 0.0 );
	}
	return res;
}

double numcpp::random( double min, double max )
{
	uniform_real_distribution<double> unif( min, max );
	return unif( *m_rng );
}

double numcpp::mean( const vector<double>& vec )
{
	double res = 0.0;
	for( const auto& val : vec )
	{
		res += val;
	}
	return res / double( vec.size() );
}

double numcpp::mean( const vector<ulong>& vec )
{
	return double( accumulate( vec.begin(), vec.end(), 0) ) / double( vec.size() );
}

// m_ncp.match( predictions, y )
vector<ulong> numcpp::match( const vector<ulong>& vec1, const vector<ulong>& vec2 )
{
	vector<ulong> res;
	for( ulong i=0; i<vec1.size(); i++ )
	{
		if( vec1[i] == vec2[i] )
		{
			res.push_back( 1 );
		} else
		{
			res.push_back( 0 );
		}
	}
	return res;
}

// convert a vector into a diaginal matrix
vector<vector<double>> numcpp::diagflat( const vector<double>& vec )
{
	vector<vector<double>> res = zeros( vec.size(), vec.size() );
	for( ulong i=0; i<vec.size(); i++ )
	{
		for( ulong k=0; k<vec.size(); k++ )
		{
			if( i == k )
			{
				res[i][k] = vec[i];
			}
		}
	}
	return res;
}

// fill a matrix with zeros.
vector<vector<double>> numcpp::zeros( ulong rows, ulong cols )
{
	// create vector of 0's
	vector<double> tmp;
	for( ulong i=0; i<rows; i++ )
	{
		tmp.push_back( 0.0 );
	}
	
	vector<vector<double>> res;
	for( ulong k=0; k<cols; k++ )
	{
		res.push_back( tmp );
	}
	return res;
}

double numcpp::accuracy( const vector<vector<double>>& inputs, const vector<ulong>& y )
{
	double res = 0.0;
	
	// predictions = np.argmax(loss_activation.inputs, axis=1)
	vector<ulong> predictions;
	for( const auto& vec : inputs )
	{
		predictions.push_back( argmax( vec ) );
	}
	// debug
	// m_db.print( "predictions", predictions );
	
	// find match
	vector<ulong> tmp;
	for( ulong i=0; i<predictions.size(); i++ )
	{
		if( predictions[i] == y[i] )
		{
			tmp.push_back( 1 );
		} else
		{
			tmp.push_back( 0 );
		}
	}
	// debug
	// m_db.print( "tmp matches", tmp );
	
	// accuracy = np.mean(predictions==y)
	res = mean( tmp ) * 100.0;
	
	return res;
}

bool numcpp::check_doubles( double val1, double val2, double factor )
{
    return fabs( val1 - val2 ) < factor;
}


//****************
// One-Hot

vector<ulong> numcpp::onehot2sparse( const vector<vector<ulong>>& y_true )
{
	// y_true is one-hot, so convert it.
	vector<ulong> new_y;
	for( const auto& vec : y_true )
	{
		new_y.push_back( argmax( vec ) );
	}
	return new_y;
}

// m_ncp.sparse2onehot( y );
vector<vector<ulong>> numcpp::sparse2onehot( const vector<ulong>& y, ulong s )
{
	vector<vector<ulong>> y_true;
	for( const auto& val : y )
	{
		vector<ulong> tmp;
		for( ulong i=0; i<s; i++ )
		{
			if( i == val )
			{
				tmp.push_back( 1 );
			} else
			{
				tmp.push_back( 0 );
			}
		}
		y_true.push_back( tmp );
	}
	return y_true;
}


//****************
// Debug ONLY

void numcpp::match( const vector<vector<double>>& mat1, const vector<vector<double>>& mat2 )
{
	// check shape.
	if( mat1.size() != mat2.size() )
	{
		cerr << "ERROR: numcpp::match : matrix shape missmatch." << endl;
		cerr << "matrix shape mat1: " << mat1.size() << "x" << mat1[0].size();
		cerr << ", mat2: " << mat2.size() << "x" << mat2[0].size() << endl;
		return;
	}
	
	// rows
	for( ulong i=0; i<mat1.size(); i++ )
	{
		for( ulong k=0; k<mat1[0].size(); k++ )
		{
			if(! check_doubles( mat1[i][k], mat2[i][k] ) )
			{
				cerr << "ERROR: Values donot match : " << mat1[i][k] << " != " << mat2[i][k] << endl;
				return;
			}
		}
	}
	cout << "### PASS ###" << endl;
}


void numcpp::match( const vector<double>& vec1, const vector<double>& vec2 )
{
	// check shape.
	if( vec1.size() != vec2.size() )
	{
		cerr << "ERROR: numcpp::match : vector size missmatch." << endl;
		cerr << "vec1 : " << vec1.size() << ", vec2 : " << vec2.size() << endl;
	}
	
	for( ulong i=0; i<vec1.size(); i++ )
	{
		if(! check_doubles( vec1[i], vec2[i] ) )
		{
			cerr << "ERROR: Values donot match." << endl;
			return;
		}
	}
	cout << "### PASS ###" << endl;
}

