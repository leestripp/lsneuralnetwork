#include "numcpp_thread.h"

numcpp_thread::numcpp_thread()
{
}

numcpp_thread::~numcpp_thread()
{
}

//**************************
// Vector



//**************************
// Matrix

// Subtract matrix - matrix.

void numcpp_thread::subtract_mm()
{
	for( ulong i=0; i<m_mat1.size(); i++ )
	{
		vector<double> tmp( m_mat1[0].size() );
		for( ulong k=0; k<m_mat1[0].size(); k++ )
		{
			tmp[k] = m_mat1[i][k] - m_mat2[i][k];
		}
		m_res_mat.push_back( tmp );
	}
}

// Add matrix + vector

void numcpp_thread::add_mv()
{
	// rows
	for( const auto& vec2 : m_mat1 )
	{
		vector<double> tmp( m_vec1.size() );
		for( ulong i=0; i<m_vec1.size(); i++ )
		{
			tmp[i] = vec2[i] + m_vec1[i];
		}
		m_res_mat.push_back( tmp );
	}
}


// row . col : matrix . matrix

void numcpp_thread::dot_mm()
{
	// iterate rows of mat1
	for( const auto& row : m_mat1 )
	{
		vector<double> tmp_row;
		// for each col of mat2
		for( ulong m2c=0; m2c<m_mat2[0].size(); m2c++ )
		{
			double val=0.0;
			for( ulong k=0; k<row.size(); k++ )
			{
				val += row[k] * m_mat2[k][m2c];
			}
			tmp_row.push_back( val );
		}
		m_res_mat.push_back( tmp_row );
	}
}

