#ifndef NUMCPP_THREAD_H
#define NUMCPP_THREAD_H

#include <iostream>
#include <thread>
#include <vector>

using namespace std;

class numcpp_thread
{
public:
	numcpp_thread();
	virtual ~numcpp_thread();
	
	// Thread calls.
	void dot_mm();
	void add_mv();
	void subtract_mm();
	
	// inputs
	vector<vector<double>> m_mat1;
	vector<vector<double>> m_mat2;
	vector<double> m_vec1;
	vector<double> m_vec2;
	double m_val;
	
	// outputs
	vector<vector<double>> m_res_mat;
	vector<double> m_res_vec;
};

#endif // NUMCPP_THREAD_H

