#ifndef NUMCPP_H
#define NUMCPP_H

#include <iostream>
#include <vector>
#include <random>
// debug
#include "lsDebug.h"

// protos
class numcpp_thread;

// add matrix
const int NCP_AXIS_ALL	= 0;
const int NCP_AXIS_ROW	= 1;
const int NCP_AXIS_COL	= 2;

using namespace std;

class numcpp
{
public:
	numcpp();
	virtual ~numcpp();
	
	// Vector
	double add( const vector<double>& vec );
	vector<double> add( const vector<double>& vec1, const vector<double>& vec2 );
	vector<double> subtract( const vector<double>& vec1, const vector<double>& vec2 );
	vector<double> mult( const vector<double>& vec, double val );
	double dot( const vector<double>& vec1, const vector<double>& vec2 );
	vector<double> normalise( const vector<double>& vec );
	
	// matrix
	vector<double> add( const vector<vector<double>>& mat, int axis=NCP_AXIS_ROW );
	vector<vector<double>> add( const vector<vector<double>>& mat, const vector<double>& vec );
	vector<vector<double>> add( const vector<vector<double>>& mat1, const vector<vector<double>>& mat2 );
	vector<vector<double>> subtract( const vector<vector<double>>& mat1, const vector<vector<double>>& mat2 );
	vector<vector<double>> mult( const vector<double>& vec1, const vector<double>& vec2 );
	vector<vector<double>> mult( const vector<vector<double>>& mat, double val );
	vector<vector<double>> mult( const vector<vector<double>>& mat1, const vector<vector<double>>& mat2 );
	vector<vector<double>> dot( const vector<vector<double>>& mat1, const vector<vector<double>>& mat2 );
	vector<double> dot( const vector<vector<double>>& mat, const vector<double>& vec );
	vector<vector<double>> transpose( const vector<vector<double>>& mat );
	vector<vector<double>> normalise_m1_1( const vector<vector<double>>& mat, bool use_min=true );
	
	// utilities
	double random( double min, double max );
	vector<double> max( const vector<double>& vec, double m );
	tuple<double, double> find_min_max( const vector<double>& vec );
	double find_max( const vector<double>& vec );
	ulong find_max( const vector<ulong>& vec );
	ulong argmax( const vector<ulong>& vec );
	ulong argmax( const vector<double>& vec );
	double clip( double n, double lower, double upper );
	double mean( const vector<double>& vec );
	double mean( const vector<ulong>& vec );
	vector<ulong> match( const vector<ulong>& vec1, const vector<ulong>& vec2 );
	vector<vector<double>> diagflat( const vector<double>& vec );
	vector<vector<double>> zeros( ulong rows, ulong cols );
	double accuracy( const vector<vector<double>>& inputs, const vector<ulong>& y );
	vector<double> normalise_0_1( const vector<double>& vec );
	vector<double> normalise_m1_1( const vector<double>& vec );
	bool check_doubles( double val1, double val2, double factor=1.0e-07 );
	
	// One-Hot
	vector<ulong> onehot2sparse( const vector<vector<ulong>>& y_true );
	vector<vector<ulong>> sparse2onehot( const vector<ulong>& y, ulong s );
	
	// debug ONLY
	void match( const vector<vector<double>>& mat1, const vector<vector<double>>& mat2 );
	void match( const vector<double>& vec1, const vector<double>& vec2 );
	
	// threaded funcs
	// Matrix
	vector<vector<double>> dot_threaded( const vector<vector<double>>& mat1, const vector<vector<double>>& mat2 );
	vector<vector<double>> add_threaded( const vector<vector<double>>& mat, const vector<double>& vec );
	vector<vector<double>> subtract_threaded( const vector<vector<double>>& mat1, const vector<vector<double>>& mat2 );
	
	// constants
	const double EULER = pow( (1.0 + 1.0 / 10000000.0), 10000000.0 );
	const double PI = 3.14159265359;
	
	// Threads.
	ulong m_thread_count;
	ulong m_block_size;
	vector<numcpp_thread *> m_ncp_threads;
	
	// Random
	random_device m_rd;
	default_random_engine *m_rng;
	
	// debug
	lsDebug m_db;
};

#endif // NUMCPP_H