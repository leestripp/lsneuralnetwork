#ifndef LSMODELSETTINGS_H
#define LSMODELSETTINGS_H

#include <iostream>
#include <vector>
#include <iomanip>
#include <format>

// numcpp
#include "numcpp.h"

// Modes
const int LSM_NONE				= 0;
const int LSM_LEARN				= 1;
const int LSM_TRAIN_VALIDATE	= 2;
const int LSM_PREDICT			= 3;

using namespace std;

class lsModelSettings
{
public:
	lsModelSettings();
	
	void add_iterations( ulong val );
	// save.
	string save();
	
	// debug
	void print();
	
	// get, set
	ulong get_current_iterations()
	{
		return m_current_iterations;
	}
	void set_current_iterations( ulong val )
	{
		m_current_iterations = val;
	}
	
	// mode
	int m_mode;
	// filename
	string m_model_filename;
	// training Settings.
	ulong m_iterations;
	ulong m_epoch_update;
	double m_learning_rate;
	double m_decay;
	double m_momentum;
	ulong m_hidden_neurons;
	double m_dropout_factor;
	
	// training info
	double m_accuracy;
	double m_loss;
	
private:
	ulong m_current_iterations;
};

#endif // LSMODELSETTINGS_H
