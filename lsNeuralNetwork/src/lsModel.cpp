#include "lsModel.h"

// layers
#include "lsLayer_dense.h"
#include "lsLayer_dropout.h"
// Activations
#include "lsActivation_Base.h"
// Optimizer
#include "lsOptimizer_SGD.h"
// neural network
#include "lsNeuralNetwork.h"


lsModel::lsModel() : lsThread()
{
	// model directory
	m_model_dir = "/tmp/.lsNeuralNetwork";
	
	m_neuralnet = NULL;
	m_class = 0;
	m_stop = false;
}

lsModel::~lsModel()
{
	if( m_neuralnet )
	{
		// cleanup
		delete m_neuralnet;
		m_neuralnet = NULL;
	}
}

//********************
// data set get mutex protected.

void lsModel::set_X( const vector<vector<double>>& X )
{
	m_data_mutex.lock();
	m_X = X;
	m_data_mutex.unlock();
}

void lsModel::get_X( vector<vector<double>>& X )
{
	m_data_mutex.lock();
	X = m_X;
	m_data_mutex.unlock();
}

void lsModel::set_y( const vector<ulong>& y )
{
	m_data_mutex.lock();
	m_y = y;
	m_data_mutex.unlock();
}

void lsModel::get_y( vector<ulong>& y )
{
	m_data_mutex.lock();
	y = m_y;
	m_data_mutex.unlock();
}

void lsModel::clear_data()
{
	m_X.clear();
	m_y.clear();
}

// test data

void lsModel::set_test_X( const vector<vector<double>>& X )
{
	m_data_mutex.lock();
	m_test_X = X;
	m_data_mutex.unlock();
}

void lsModel::get_test_X( vector<vector<double>>& X )
{
	m_data_mutex.lock();
	X = m_test_X;
	m_data_mutex.unlock();
}

void lsModel::set_test_y( const vector<ulong>& y )
{
	m_data_mutex.lock();
	m_test_y = y;
	m_data_mutex.unlock();
}

void lsModel::get_test_y( vector<ulong>& y )
{
	m_data_mutex.lock();
	y = m_test_y;
	m_data_mutex.unlock();
}

void lsModel::clear_test_data()
{
	m_test_X.clear();
	m_test_y.clear();
}

//********************
// Work Thread.

void lsModel::work()
{
	// debug
	// cout << "Thread: starting work..." << endl;
	
	// Clear old data.
	m_stats.clear();
	
	// Check mode and start work.
	switch( m_settings.m_mode )
	{
		case LSM_NONE:
		{
			cerr << "ERROR: No mode set on model..." << endl;
			break;
		}
		
		case LSM_LEARN:
		{
			// debug
			// cout << "Mode: LSM_LEARN" << endl;
			learn();
			break;
		}
		
		case LSM_TRAIN_VALIDATE:
		{
			// debug
			// cout << "Mode: LSM_TRAIN_VALIDATE" << endl;
			train();
			break;
		}
		
		case LSM_PREDICT:
		{
			// debug
			// cout << "Mode: LSM_PREDICT" << endl;
			predict();
			break;
		}
		
		default:
		{
			cerr << "ERROR: Unknown mode set on model..." << endl;
			break;
		}
	}
	
	// signal we are done.
	set_done(true);
	notify_done.emit( this );
}

// Learn
// Create new ndtwork OR
// load the old model and continue training.
// track iterations. stored in m_settings.

void lsModel::learn()
{
	// reset values
	m_stop = false;
	
	// Check learning data.
	if( m_X.empty() || m_y.empty() )
	{
		cerr << "ERROR: Learning data empty." << endl;
		return;
	}
	
	// ****************
	// Neural network
	if(! m_neuralnet )
	{
		load();
		if(! m_neuralnet )
		{
			// debug
			cout << "Creating new neural network for model." << endl;
			
			// reset values.
			m_settings.set_current_iterations(0);
			
			// Create new network.
			m_neuralnet = new lsNeuralNetwork;
			if(! m_neuralnet )
			{
				cerr << "ERROR: Could not create neural network instance." << endl;
				return;
			}
			
			// Calc network size.
			ulong num_inputs = m_X[0].size();
			ulong num_outputs = m_ncp.find_max( m_y ) + 1;
			
			m_neuralnet->add_layer( new lsLayer_dense( num_inputs, m_settings.m_hidden_neurons, LSAT_ReLU ) );
			m_neuralnet->add_layer( new lsLayer_dropout( m_settings.m_dropout_factor ) );
			m_neuralnet->add_layer( new lsLayer_dense( m_settings.m_hidden_neurons, num_outputs, LSAT_Softmax_LossCCE ) );
			// Optimizer SGD-M
			m_neuralnet->m_optimizer = new lsOptimizer_SGD( m_settings.m_learning_rate, m_settings.m_decay, m_settings.m_momentum );
		}
	}
	
	// settings.
	// dropouts needed.
	m_neuralnet->set_dropout_enabled( true );
	m_neuralnet->set_activation_function( LSAT_Softmax_LossCCE );
	
	// Copy data to neuralnet.
	m_neuralnet->m_X = m_X;
	m_neuralnet->m_y = m_y;
	
	// **********
	// Main loop
	
	for( ulong epoch=1; epoch<=m_settings.m_iterations; epoch++ )
	{
		if( m_stop )
		{
			cout << "User stopped the learning process." << endl;
			return;
		}
		
		// Forward pass
		auto[ loss, acc ] = m_neuralnet->forward();
		m_settings.add_iterations(1);
		// Store in Model settings.
		m_settings.m_accuracy = acc;
		m_settings.m_loss = loss;
		
		// debug and lsGraph
		// save stats: normalise -1<>1
		double tmp_loss = (loss*2.0)-1.0;
		double tmp_acc = ((acc/100.0)*2.0)-1.0;
		add_stats( vector<double> { tmp_loss, tmp_acc } );
		
		// Add progress stats.
		// epoch update
		if( epoch % m_settings.m_epoch_update == 0 )
		{
			// debug
			cout << "Epoch : " << m_settings.get_current_iterations() << ", ";
			cout << "Accuracy : % " << setprecision(2) << fixed << m_settings.m_accuracy << ", ";
			cout << "Loss : " << setprecision(10) << fixed << m_settings.m_loss << endl;
			
			// update
			notify_update.emit( this );
		}
		
		// backward pass
		m_neuralnet->backward();
		// Optimize
		m_neuralnet->optimize();
		
	} // main loop
	// debug
	// cout << endl;
	
	//***************
	// Validate
	
	// Check training data.
	if( m_test_X.empty() || m_test_y.empty() )
	{
		// debug
		// cout << "Skipping Validation." << endl;
		return;
	}
	
	// Copy test data to model.
	m_neuralnet->m_X = m_test_X;
	m_neuralnet->m_y = m_test_y;
	
	// disable dropout layers.
	m_neuralnet->set_dropout_enabled( false );
	// Forward pass
	auto[ loss, acc ] = m_neuralnet->forward();
	// debug
	cout << "Validation: Accuracy : % " << setprecision(2) << fixed << acc << ", ";
	cout << "Loss : " << setprecision(10) << fixed << loss << endl;
}

// train
// train a model from scratch.

void lsModel::train()
{
	// reset values
	m_stop = false;
	m_settings.set_current_iterations(0);
	
	// Check training data.
	if( m_X.empty() || m_y.empty() )
	{
		cerr << "ERROR: Training data empty." << endl;
		return;
	}
	
	// ****************
	// Neural network
	
	// train always starts from a fresh Network.
	if( m_neuralnet )
	{
		delete m_neuralnet;
	}
	m_neuralnet = new lsNeuralNetwork;
	if(! m_neuralnet )
	{
		cerr << "ERROR: Could not create neural network instance." << endl;
		return;
	}
	// Calc network size.
	ulong num_inputs = m_X[0].size();
	ulong num_outputs = m_ncp.find_max( m_y ) + 1;
	
	m_neuralnet->add_layer( new lsLayer_dense( num_inputs, m_settings.m_hidden_neurons, LSAT_ReLU ) );
	m_neuralnet->add_layer( new lsLayer_dropout( m_settings.m_dropout_factor ) );
	m_neuralnet->add_layer( new lsLayer_dense( m_settings.m_hidden_neurons, num_outputs, LSAT_Softmax_LossCCE ) );
	// Optimizer SGD-M
	m_neuralnet->m_optimizer = new lsOptimizer_SGD( m_settings.m_learning_rate, m_settings.m_decay, m_settings.m_momentum );
	
	//****************************
	// Copy data to neuralnet.
	
	m_neuralnet->m_X = m_X;
	m_neuralnet->m_y = m_y;
	
	// **********
	// Main loop
	
	for( ulong epoch=1; epoch<=m_settings.m_iterations; epoch++ )
	{
		if( m_stop )
		{
			cout << "User stopped the training process." << endl;
			return;
		}
		
		// Forward pass
		auto[ loss, acc ] = m_neuralnet->forward();
		m_settings.add_iterations(1);
		// Store in Model settings.
		m_settings.m_accuracy = acc;
		m_settings.m_loss = loss;
		
		// debug and lsGraph.
		// save stats: normalise -1<>1
		double tmp_loss = (loss*2.0)-1.0;
		double tmp_acc = ((acc/100.0)*2.0)-1.0;
		add_stats( vector<double> { tmp_loss, tmp_acc } );
		
		// Add progress stats.
		if( epoch % m_settings.m_epoch_update == 0 )
		{
			// debug
			cout << "Epoch : " << epoch << ", ";
			cout << "Accuracy : % " << setprecision(2) << fixed << acc << ", ";
			cout << "Loss : " << setprecision(10) << fixed << loss << endl;
			
			// update
			notify_update.emit( this );
		}
		
		// backward pass
		m_neuralnet->backward();
		// Optimize
		m_neuralnet->optimize();
		
	} // main loop
	// debug
	// cout << endl;
	
	//***************
	// Validate
	
	// Check training data.
	if( m_test_X.empty() || m_test_y.empty() )
	{
		// debug
		// cout << "Skipping Validation." << endl;
		return;
	}
	
	// Copy test data to model.
	m_neuralnet->m_X = m_test_X;
	m_neuralnet->m_y = m_test_y;
	
	// disable dropout layers.
	m_neuralnet->set_dropout_enabled( false );
	// Forward pass
	auto[ loss, acc ] = m_neuralnet->forward();
	// debug
	cout << "Validation: Accuracy : % " << setprecision(2) << fixed << acc << ", ";
	cout << "Loss : " << setprecision(10) << fixed << loss << endl;
}

// Predict

void lsModel::predict()
{
	if( m_X.empty() )
	{
		cerr << "ERROR: Predict, X is empty." << endl;
		return;
	}
	
	// ****************
	// Neural network
	if(! m_neuralnet )
	{
		load();
		if(! m_neuralnet )
		{
			cerr << "ERROR: Could not load the model." << endl;
			return;
		}
	}
	
	// clear old data
	m_outputs.clear();
	
	// settings.
	m_neuralnet->set_activation_function( LSAT_Softmax );
	// disable dropout layers.
	m_neuralnet->set_dropout_enabled( false );
	
	// Copy data to neuralnet.
	m_neuralnet->m_X = m_X;
	
	// forward.
	m_neuralnet->forward();
	
	// store outputs.
	m_outputs = m_neuralnet->get_Last_Layer()->m_outputs[0];
	m_class = m_neuralnet->get_class();
	// Convert and Add to stats
	for( const auto& val : m_outputs )
	{
		add_stats( vector<double> { val } );
	}
}

//**********************
// Save, load and delete

void lsModel::save()
{
	if( m_settings.m_model_filename.empty() ) return;
	
	// Make sure our folder exist.
	string full_path = m_model_dir;
	filesystem::path p( full_path );
	if(! filesystem::exists( p ) )
	{
		cout << "Creating folder : " << full_path << endl;
		filesystem::create_directories( p );
	}
	
	// open file
	full_path = m_model_dir + "/" + m_settings.m_model_filename;
	ofstream fs( full_path );
	if(! fs.is_open() )
	{
		cerr << "ERROR: Could not open file for saving : " << m_settings.m_model_filename << endl;
		return;
	}
	
	// debug
	cout << "Saving model file : " << full_path << endl;
	
	// save settings.
	fs << m_settings.save() << endl;
	if( m_neuralnet )
	{
		fs << m_neuralnet->save() << endl;
	}
	fs.close();
}

// Load.

void lsModel::load()
{
	if( m_settings.m_model_filename.empty() ) return;
	
	string full_path = m_model_dir + "/" + m_settings.m_model_filename;
	// debug
	cout << "Loading model file : " << full_path << endl;
	ifstream mf( full_path );
	if(! mf.is_open() )
	{
		cerr << "ERROR: Could not open model file : " << full_path << endl;
		return;
	}
	
	// create the network.
	lsNeuralNetwork *nn = new lsNeuralNetwork;
	if(! nn )
	{
		cerr << "ERROR: Could not create neural network instance." << endl;
		return;
	}
	
	// Settings
	lsLayer_Base *layer = NULL;
	// read line by line
	for( string line; getline( mf, line ); )
	{
		if( line.empty() ) continue;
		
		// Split by space
		vector<string> line_split = split( line, " " );
		
		if( line_split[0] == "layer_dense" )
		{
			// debug
			// cout << "Adding lsLayer_dense" << endl;
			
			// Activation function
			ulong af =  stoul(line_split[3]);
			
			// e.g. layer_dense 400 8 LSAT_ReLU
			layer = new lsLayer_dense( stoul(line_split[1]), stoul(line_split[2]), af );
			nn->add_layer( layer );
			// Clear preloaded weights and biases.
			layer->m_weights.clear();
			layer->m_biases.clear();
			
		} else if( line_split[0] == "layer_dropout" )
		{
			// debug
			// cout << "Adding lsLayer_dropout" << endl;
			
			// Add a dropout layer
			layer = new lsLayer_dropout( stod(line_split[1]), stoul(line_split[2]) );
			if( layer )
			{
				nn->add_layer( layer );
			}
			
		} else if( line_split[0] == "weights" )
		{
			vector<double> tmp;
			
			// split weights by comma.
			vector<string> weights = split( line_split[1], "," );
			for( const auto& val : weights )
			{
				tmp.push_back( stod( val ) );
			}
			layer->m_weights.push_back( tmp );
			
		} else if( line_split[0] == "biases" )
		{
			vector<double> tmp;
			
			// split weights by comma.
			vector<string> biases = split( line_split[1], "," );
			for( const auto& val : biases )
			{
				tmp.push_back( stod( val ) );
			}
			layer->m_biases = tmp;
			
		} else if( line_split[0] == "current_iterations" )
		{
			m_settings.set_current_iterations( stoul(line_split[1]) );
			
		} else if( line_split[0] == "learning_rate" )
		{
			m_settings.m_learning_rate = stod(line_split[1]);
		} else if( line_split[0] == "decay" )
		{
			m_settings.m_decay = stod(line_split[1]);
		} else if( line_split[0] == "momentum" )
		{
			m_settings.m_momentum = stod(line_split[1]);
		}
		
	} // for readline
	
	// Optimizer SGD-M
	nn->m_optimizer = new lsOptimizer_SGD( m_settings.m_learning_rate, m_settings.m_decay, m_settings.m_momentum );
	
	// cleanup
	mf.close();
	
	// set net.
	m_neuralnet = nn;
	m_settings.print();
}

void lsModel::delete_model()
{
	if( m_settings.m_model_filename.empty() ) return;
	
	string full_path = m_model_dir + "/" + m_settings.m_model_filename;
	remove( full_path.c_str() );
}


//*****************
// Data

void lsModel::add_stats( const vector<double>& stats )
{
	m_stats_mutex.lock();
	m_stats.push_back( stats );
	m_stats_mutex.unlock();
}

vector<vector<double>> lsModel::get_stats()
{
	return m_stats;
}

// Utilities

vector<string> lsModel::split( string str, const string& token )
{
    vector<string>result;
	
	if( str.empty() )
	{
		cerr << "ERROR: lsModel::split : empty string" << endl;
		return result;
	}
	
    while( str.size() )
	{
        ulong index = str.find( token );
        if( index != string::npos )
		{
            result.push_back(str.substr(0,index));
            str = str.substr(index+token.size());
            if( str.size() == 0 )
			{
				result.push_back( str );
			}
        } else
		{
            result.push_back( str );
            str = "";
        }
    }
    return result;
}

