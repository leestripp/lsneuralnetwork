#ifndef LSACTIVATION_BASE_H
#define LSACTIVATION_BASE_H

#include <iostream>
#include <vector>

// numcpp
#include "numcpp.h"
// debug
#include "lsDebug.h"

const ulong LSAT_NONE				= 0;
const ulong LSAT_ReLU				= 1;
const ulong LSAT_Softmax			= 2;
const ulong LSAT_Softmax_LossCCE	= 3;

using namespace std;

class lsActivation_Base
{
public:
	lsActivation_Base();
	virtual ~lsActivation_Base();
	
	// overrides
	virtual void forward( const vector<vector<double>>& inputs ) = 0;
	virtual void backward( const vector<vector<double>>& dvalues ) = 0;
	
	// class settings.
	ulong m_activation_type;
	
	// forward pass
	// outputs
	vector<vector<double>> m_outputs;
	vector<vector<double>> m_inputs;
	// backward pass
	vector<vector<double>> m_dinputs;
	
	// numcpp
	numcpp m_ncp;
	// debug
	lsDebug m_db;
};

#endif // LSACTIVATION_BASE_H