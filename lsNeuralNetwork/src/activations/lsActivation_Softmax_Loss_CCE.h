#ifndef LSACTIVATION_SOFTMAX_LOSS_CCE_H
#define LSACTIVATION_SOFTMAX_LOSS_CCE_H

#include <iostream>

#include "lsActivation_Softmax.h"
#include "lsActivation_Base.h"
// losses
#include "lsLoss_CCE.h"

using namespace std;

class lsActivation_Softmax_Loss_CCE : public lsActivation_Base
{
public:
	lsActivation_Softmax_Loss_CCE();
	virtual ~lsActivation_Softmax_Loss_CCE();
	
	double calculate( const vector<vector<double>>& inputs, const vector<ulong>& y );
	void backward_Y( const vector<vector<double>>& dvalues, const vector<ulong>& y );
	
	// overrides.
	void forward( const vector<vector<double>>& inputs );
	void backward( const vector<vector<double>>& dvalues );
	
	lsActivation_Softmax activation;
	lsLoss_CCE loss;
	
};

#endif // LSACTIVATION_SOFTMAX_LOSS_CCE_H

