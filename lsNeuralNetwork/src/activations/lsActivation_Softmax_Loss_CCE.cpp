#include "lsActivation_Softmax_Loss_CCE.h"

lsActivation_Softmax_Loss_CCE::lsActivation_Softmax_Loss_CCE()
{
	m_activation_type = LSAT_Softmax_LossCCE;
}

lsActivation_Softmax_Loss_CCE::~lsActivation_Softmax_Loss_CCE()
{
}

void lsActivation_Softmax_Loss_CCE::forward( __attribute__((unused)) const vector<vector<double>>& inputs )
{
	// debug
	cerr << "ERROR: lsActivation_Softmax_Loss_CCE::forward" << endl;
	// Not used.
}

void lsActivation_Softmax_Loss_CCE::backward( __attribute__((unused)) const vector<vector<double>>& dvalues )
{
	// debug
	cerr << "ERROR: lsActivation_Softmax_Loss_CCE::backward" << endl;
	// Not used.
}


double lsActivation_Softmax_Loss_CCE::calculate( const vector<vector<double>>& inputs, const vector<ulong>& y )
{
	// debug
	// cout << "lsActivation_Softmax_Loss_CCE::calculate" << endl;
	
	// Output layer's activation function
	activation.forward( inputs );
	// Set the output
	m_outputs = activation.m_outputs;
	
	// Calculate and return loss value
	return loss.calculate( m_outputs, y );
}

void lsActivation_Softmax_Loss_CCE::backward_Y( const vector<vector<double>>& dvalues, const vector<ulong>& y )
{
	// debug
	// cout << "lsActivation_Softmax_Loss_CCE::backward" << endl;
	
	// cleanup last pass
	m_dinputs.clear();
	
	// Number of samples
	ulong samples = dvalues.size();
	
	// Calculate gradient
	// self.dinputs[range(samples), y_true] -= 1
	vector<vector<double>> tmp_dinputs;
	for( ulong i=0; i<dvalues.size(); i++ )
	{
		vector<double> tmp = dvalues[i];
		tmp[ y[i] ] -= 1;
		tmp_dinputs.push_back( tmp );
	}
	
	// Normalize gradient
	// m_dinputs = m_dinputs / samples
	for( ulong i=0; i<tmp_dinputs.size(); i++ )
	{
		vector<double> tmp;
		for( ulong k=0; k<tmp_dinputs[0].size(); k++ )
		{
			tmp.push_back( tmp_dinputs[i][k] / samples );
		}
		m_dinputs.push_back( tmp );
	}
}

