#ifndef LSACTIVATION_RELU_H
#define LSACTIVATION_RELU_H

#include <iostream>

// Base class
#include "lsActivation_Base.h"

using namespace std;

class lsActivation_ReLU : public lsActivation_Base
{
public:
	lsActivation_ReLU();
	virtual ~lsActivation_ReLU();
	
	void forward( const vector<vector<double>>& inputs );
	void backward( const vector<vector<double>>& dvalues );
	
};

#endif // LSACTIVATION_RELU_H
