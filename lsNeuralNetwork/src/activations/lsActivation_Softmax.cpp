#include "lsActivation_Softmax.h"

lsActivation_Softmax::lsActivation_Softmax()
{
	m_activation_type = LSAT_Softmax;
}

lsActivation_Softmax::~lsActivation_Softmax()
{
}

void lsActivation_Softmax::forward( const vector<vector<double>>& inputs )
{
	// debug
	// cout << "lsActivation_Softmax::forward" << endl;
	
	// cleanup last pass
	m_outputs.clear();
	
	for( const auto& vec : inputs )
	{
		vector<double> data, exp_data, res;
		
		// sub max
		double m = m_ncp.find_max( vec );
		for( const auto& val : vec )
		{
			data.push_back( val - m );
		}
		
		// euler exp
		for( const auto& val : data )
		{
			exp_data.push_back( pow( m_ncp.EULER, val ) );
		}
		
		// normalise
		res = m_ncp.normalise( exp_data );
		
		m_outputs.push_back( res );
	}
}

void lsActivation_Softmax::backward( const vector<vector<double>>& dvalues )
{
	// debug
	// cout << "lsActivation_Softmax::backward" << endl;
	
	// cleanup last pass
	m_dinputs.clear();
	
	// Iterate outputs and gradients
	for( ulong i=0; i<dvalues.size(); i++ )
	{
		vector<vector<double>> m = m_ncp.mult( m_outputs[i], m_outputs[i] );
		// m_db.print( "m", m );
		
		vector<vector<double>> jacobian_matrix;
		jacobian_matrix = m_ncp.diagflat( m_outputs[i] );
		jacobian_matrix = m_ncp.subtract( jacobian_matrix, m );
		// m_db.print( "jacobian_matrix", jacobian_matrix );
		
		vector<double> tmp = m_ncp.dot( jacobian_matrix, dvalues[i] );
		m_dinputs.push_back( tmp );
	}
	// debug
	// m_db.print( "m_dinputs", m_dinputs );
}
