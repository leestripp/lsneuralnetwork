#include "lsActivation_ReLU.h"

lsActivation_ReLU::lsActivation_ReLU()
{
	m_activation_type = LSAT_ReLU;
}

lsActivation_ReLU::~lsActivation_ReLU()
{
}

void lsActivation_ReLU::forward( const vector<vector<double>>& inputs )
{
	// debug
	// cout << "lsActivation_ReLU::forward" << endl;
	
	// cleanup last pass
	m_outputs.clear();
	
	// Save inputs
	m_inputs = inputs;
	
	// ReLU
	for( const auto& vec : inputs )
	{
		m_outputs.push_back( m_ncp.max( vec, 0.0 ) );
	}
}

void lsActivation_ReLU::backward( const vector<vector<double>>& dvalues )
{
	// debug
	// cout << "lsActivation_ReLU::backward" << endl;
	
	// Since we need to modify the original variable,
	// let's make a copy of the values first
	m_dinputs = dvalues;
	
	// debug
	// m_db.print( "m_inputs", m_inputs );
	// m_db.print( "dvalues", dvalues );
	
	// check matrix sizes.
	if( m_inputs[0].size() != m_dinputs[0].size() )
	{
		cerr << "ERROR: lsActivation_ReLU::backward - matrix missmatch." << endl;
	}
	
	// Zero gradient where input values were negative
	ulong i=0;
	for( const auto& vec : m_inputs )
	{
		for( ulong k=0; k<vec.size(); k++ )
		{
			if( vec[k] < 0 )
			{
				m_dinputs[i][k] = 0.0;
			}
		}
		i++;
	}
	// debug
	// m_db.print( "m_dinputs", m_dinputs );
}

