#ifndef LSACTIVATION_SOFTMAX_H
#define LSACTIVATION_SOFTMAX_H

#include <iostream>

#include "lsActivation_Base.h"

using namespace std;

class lsActivation_Softmax : public lsActivation_Base
{
public:
	lsActivation_Softmax();
	virtual ~lsActivation_Softmax();
	
	void forward( const vector<vector<double>>& inputs );
	void backward( const vector<vector<double>>& dvalues );
	
};

#endif // LSACTIVATION_SOFTMAX_H
