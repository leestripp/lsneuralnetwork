#ifndef LSNEURALNETWORK_H
#define LSNEURALNETWORK_H

#include <iostream>
#include <sstream>

// numcpp
#include "numcpp.h"
// debug
#include "lsDebug.h"

// protos
class lsLayer_Base;
class lsOptimizer_SGD;

using namespace std;

class lsNeuralNetwork
{
public:
	lsNeuralNetwork();
	virtual ~lsNeuralNetwork();
	
	tuple<double, double> forward();
	void backward();
	void optimize();
	void set_dropout_enabled( bool enabled );
	void set_activation_function( ulong af );
	
	// Save
	string save();
	
	void add_layer( lsLayer_Base *layer );
	lsLayer_Base* get_Last_Layer();
	ulong get_class();
	
	// Optimizer
	lsOptimizer_SGD *m_optimizer;
	
	// data
	vector<vector<double>> m_X;
	vector<ulong> m_y;
	
private:
	lsLayer_Base *m_layer_list;
	lsLayer_Base *m_last_layer;
	
	// numcpp
	numcpp m_ncp;
	// debug
	lsDebug m_db;
};

#endif // LSNEURALNETWORK_H
