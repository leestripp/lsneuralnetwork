#ifndef LSLAYER_DENSE_H
#define LSLAYER_DENSE_H

#include <iostream>
#include <sstream>
#include <format>

// Layer basse
#include "lsLayer_Base.h"

using namespace std;

class lsLayer_dense : public lsLayer_Base
{
public:
	lsLayer_dense( ulong num_inputs, ulong num_neurons, ulong atype );
	virtual ~lsLayer_dense();
	
	void set_activation( ulong atype );
	void forward( const vector<vector<double>>& inputs );
	void backward( const vector<vector<double>>& inputs );
	void reset();
	
	// save.
	string save();
};

#endif // LSLAYER_DENSE_H
