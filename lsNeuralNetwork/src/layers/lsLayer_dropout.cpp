#include "lsLayer_dropout.h"

lsLayer_dropout::lsLayer_dropout( double rate, ulong masks ) : lsLayer_Base()
{
	m_layer_type = LSLT_DROPOUT;
	
	// On for training. off for tests and predict.
	m_enabled = true;
	
	// mask rate.
	m_rate = rate;
	
	m_max_masks = masks;
	m_current_mask = 0;
}

lsLayer_dropout::~lsLayer_dropout()
{
}


void lsLayer_dropout::forward( const vector<vector<double>>& inputs )
{
	// debug
	// cout << "lsLayer_dropout::forward" << endl;
	
	// copy inputs
	m_inputs = inputs;
	
	if( m_enabled )
	{
		// Pre-Generate and store scaled masks
		if( m_binary_masks.empty() )
		{
			// debug
			// cout << "Generating " << m_max_masks << " dropout masks." << endl;
			
			// masks.
			for( ulong m=0; m<m_max_masks; m++ )
			{
				vector<vector<double>> tmp_mask;
				
				for( ulong i=0; i<inputs.size(); i++ )
				{
					vector<double> tmp( inputs[0].size() );
					for( ulong k=0; k<inputs[0].size(); k++ )
					{
						double r = m_ncp.random( -m_rate, 1.0-m_rate );
						if( r >= 0.0 )
						{
							tmp[k] = 1.0 / ( 1.0 - m_rate );
						} else tmp[k] = 0.0;
					}
					tmp_mask.push_back( tmp );
				}
				m_binary_masks.push_back( tmp_mask );
			}
		}
		// debug
		// m_db.print( "m_binary_masks[ m_current_mask ]", m_binary_masks[ m_current_mask ] );
		// m_db.print( "inputs", inputs );
		
		// Apply mask to output values
		m_outputs = m_ncp.mult( inputs, m_binary_masks[ m_current_mask ] );
		
		// debug
		// m_db.print( "m_outputs", m_outputs );
	} else
	{
		// raw copy, layer disabled.
		m_outputs = m_inputs;
	}
	
	// Pass it on.
	if( m_next_layer )
	{
		m_next_layer->forward( m_outputs );
	}
}

void lsLayer_dropout::backward( const vector<vector<double>>& dvalues )
{
	// debug
	// cout << "lsLayer_dropout::backward" << endl;
	
	// Gradient on values
	m_dinputs = m_ncp.mult( dvalues, m_binary_masks[ m_current_mask ] );
	
	// next mask
	m_current_mask++;
	if( m_current_mask >= m_max_masks ) m_current_mask = 0;
	
	// debug
	// m_db.print( "m_dinputs", m_dinputs );
	
	// Pass it on.
	if( m_prev_layer )
	{
		m_prev_layer->backward( m_dinputs );
	}
}


void lsLayer_dropout::reset()
{
}

string lsLayer_dropout::save()
{
	string output;
	
	// layer_dropout rate_factor num_masks
	output = "layer_dropout ";
	output += to_string( m_rate );
	output += " ";
	output += to_string( m_max_masks );
	output += "\n";
	return output;
}

