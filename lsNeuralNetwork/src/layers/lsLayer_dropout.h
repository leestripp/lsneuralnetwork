#ifndef LSLAYER_DROPOUT_H
#define LSLAYER_DROPOUT_H

#include <iostream>

// Layer basse
#include "lsLayer_Base.h"

using namespace std;

class lsLayer_dropout : public lsLayer_Base
{
public:
	lsLayer_dropout( double rate=0.1, ulong masks=25 );
	virtual ~lsLayer_dropout();
	
	void forward( const vector<vector<double>>& inputs );
	void backward( const vector<vector<double>>& dvalues );
	void reset();
	
	// save.
	string save();
	
	// disable for tests and predict.
	bool m_enabled;
	
private:
	double m_rate;
	vector<vector<vector<double>>> m_binary_masks;
	ulong m_current_mask;
	ulong m_max_masks;
	
};

#endif // LSLAYER_DROPOUT_H