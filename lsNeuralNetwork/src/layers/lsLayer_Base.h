#ifndef LSLAYER_BASE_H
#define LSLAYER_BASE_H

#include <iostream>
#include <string>

// numcpp
#include "numcpp.h"
// debug
#include "lsDebug.h"

// protos
class lsActivation_Base;

const ulong LSLT_NONE		= 0;
const ulong LSLT_DENSE		= 1;
const ulong LSLT_DROPOUT	= 2;

using namespace std;

class lsLayer_Base
{
public:
	lsLayer_Base();
	virtual ~lsLayer_Base();
	
	virtual void forward( const vector<vector<double>>& inputs ) = 0;
	virtual void backward( const vector<vector<double>>& inputs ) = 0;
	virtual void reset() = 0;
	// save
	virtual string save() = 0;
	
	// class settings.
	ulong m_layer_type;
	
	// class data.
	vector<ulong> m_y;
	
	// forward pass
	// params
	vector<vector<double>> m_weights;
	vector<double> m_biases;
	// outputs
	vector<vector<double>> m_outputs;
	
	ulong m_num_inputs;
	ulong m_num_neurons;
	
	// backward pass
	// inputs
	vector<vector<double>> m_inputs;
	// backward
	vector<vector<double>> m_dweights;
	vector<double> m_dbiases;
	// Gradient on values
	vector<vector<double>> m_dinputs;
	
	// Optimizer momentums
	vector<vector<double>> m_weight_momentums;
	vector<double> m_bias_momentums;
	
	// Linked list
	lsLayer_Base *m_next_layer;
	lsLayer_Base *m_prev_layer;
	
	// Activation.
	lsActivation_Base *m_activation;
	double m_loss;
	
	// numcpp
	numcpp m_ncp;
	// debug
	lsDebug m_db;
};

#endif // LSLAYER_BASE_H