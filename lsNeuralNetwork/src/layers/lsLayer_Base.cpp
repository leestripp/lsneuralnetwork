#include "lsLayer_Base.h"

lsLayer_Base::lsLayer_Base()
{
	m_layer_type = LSLT_NONE;
	
	// Activation function.
	m_activation = NULL;
	
	// Linked list.
	m_next_layer = NULL;
	m_prev_layer = NULL;
}

lsLayer_Base::~lsLayer_Base()
{
	if( m_next_layer )
	{
		delete m_next_layer;
	}
}
