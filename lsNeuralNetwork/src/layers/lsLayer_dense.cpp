#include "lsLayer_dense.h"

// Activations
#include "lsActivation_ReLU.h"
#include "lsActivation_Softmax_Loss_CCE.h"

lsLayer_dense::lsLayer_dense( ulong num_inputs, ulong num_neurons, ulong atype ) : lsLayer_Base()
{
	m_layer_type = LSLT_DENSE;
	
	m_num_inputs = num_inputs;
	m_num_neurons = num_neurons;
	
	// weights and biases.
	reset();
	// Activation
	set_activation( atype );
}

lsLayer_dense::~lsLayer_dense()
{
	if( m_activation )
	{
		delete m_activation;
	}
}

void lsLayer_dense::set_activation( ulong atype )
{
	// Clear old activation if its not the same.
	if( m_activation )
	{
		if( m_activation->m_activation_type != atype )
		{
			delete m_activation;
			m_activation = NULL;
		} else
		{
			// same
			return;
		}
	}
	
	switch( atype )
	{
		case LSAT_ReLU:
		{
			// debug
			cout << "Setting activation function to LSAT_ReLU." << endl;
			m_activation = new lsActivation_ReLU;
			break;
		}
		
		case LSAT_Softmax:
		{
			// debug
			cout << "Setting activation function to LSAT_Softmax." << endl;
			m_activation = new lsActivation_Softmax;
			break;
		}
		
		case LSAT_Softmax_LossCCE:
		{
			// debug
			cout << "Setting activation function to LSAT_Softmax_LossCCE." << endl;
			m_activation = new lsActivation_Softmax_Loss_CCE;
			break;
		}
	}
}

void lsLayer_dense::forward( const vector<vector<double>>& inputs )
{
	// debug
	// cout << "lsLayer_dense::forward" << endl;
	
	// copy inputs
	m_inputs = inputs;
	
	m_outputs = m_ncp.dot( inputs, m_weights );
	m_outputs = m_ncp.add( m_outputs, m_biases );
	
	// debug
	// m_db.print( "outputs", m_outputs );
	
	// Activation
	if( m_activation )
	{
		switch( m_activation->m_activation_type )
		{
			case LSAT_Softmax:
			case LSAT_ReLU:
			{
				m_activation->forward( m_outputs );
				m_outputs = m_activation->m_outputs;
				break;
			}
			
			case LSAT_Softmax_LossCCE:
			{
				lsActivation_Softmax_Loss_CCE *act = (lsActivation_Softmax_Loss_CCE *)m_activation;
				m_loss = act->calculate( m_outputs, m_y );
				m_outputs = act->m_outputs;
				break;
			}
			
		}
	}
	
	// Pass it on.
	if( m_next_layer )
	{
		m_next_layer->forward( m_outputs );
	}
}

void lsLayer_dense::backward( const vector<vector<double>>& dvalues )
{
	// debug
	// cout << "lsLayer_dense::backward" << endl;
	
	// Activation
	if( m_activation )
	{
		switch( m_activation->m_activation_type )
		{
			case LSAT_Softmax:
			case LSAT_ReLU:
			{
				m_activation->backward( dvalues );
				m_dinputs = m_activation->m_dinputs;
				break;
			}
			
			case LSAT_Softmax_LossCCE:
			{
				lsActivation_Softmax_Loss_CCE *act = (lsActivation_Softmax_Loss_CCE *)m_activation;
				act->backward_Y( dvalues, m_y );
				m_dinputs = act->m_dinputs;
				break;
			}
		}
	}
	
	// Gradients on parameters
	m_dweights = m_ncp.dot( m_ncp.transpose( m_inputs ), m_dinputs );
	m_dbiases = m_ncp.add( m_dinputs, NCP_AXIS_COL );
	
	// Gradient on values
	m_dinputs = m_ncp.dot( m_dinputs, m_ncp.transpose( m_weights ) );
	
	// debug
	// m_db.print( "m_dinputs", m_dinputs );
	
	// Pass it on.
	if( m_prev_layer )
	{
		m_prev_layer->backward( m_dinputs );
	}
}


void lsLayer_dense::reset()
{
	// clear old data.
	m_weights.clear();
	m_weight_momentums.clear();
	m_biases.clear();
	m_bias_momentums.clear();
	
	// Create random weights.
	for( ulong i=0; i<m_num_inputs; i++ )
	{
		vector<double> w, wm;
		for( ulong k=0; k<m_num_neurons; k++ )
		{
			w.push_back( 0.01 * m_ncp.random( -2.0, 2.0 ) );
			
			// optimizer
			wm.push_back( 0.0 );
		}
		m_weights.push_back( w );
		// optimizer
		m_weight_momentums.push_back( wm );
	}
	
	// Biases
	for( ulong k=0; k<m_num_neurons; k++ )
	{
		m_biases.push_back( 0.0 );
	}
	m_bias_momentums = m_biases;
}

string lsLayer_dense::save()
{
	stringstream output;
	
	output << "layer_dense ";
	output << format( "{}", m_num_inputs );
	output << " ";
	output << format( "{}", m_num_neurons );
	output << " ";
	if( m_activation )
	{
		output << format( "{}", m_activation->m_activation_type );
	}
	output << endl;
	
	// Add weights and biases.
	for( const auto& vec : m_weights )
	{
		output << "weights ";
		for( ulong i=0; i<vec.size(); i++ )
		{
			// use float to avoid out_of_range
			output << format( "{}", (float)vec[i] );
			if( i != vec.size() - 1 )
			{
				output << ",";
			}
		}
		output << endl;
	}
	
	output << "biases ";
	for( ulong i=0; i<m_biases.size(); i++ )
	{
		// use float to avoid out_of_range
		output << format( "{}", (float)m_biases[i] );
		if( i != m_biases.size() - 1 )
		{
			output << ",";
		}
	}
	output << endl;
	
	return output.str();
}

