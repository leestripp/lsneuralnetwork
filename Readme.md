# lsNN v0.4-alpha
## License: MIT

A C++ Neural Network library.

## CPU only. Not for production use.

![image](screenshots/lsNN_data.png)
Shows the target pattern (line) in the random noise data.

![image](screenshots/lsNN_train.png)
Training run.

![image](screenshots/lsNN_predict.png)
Prediction, steeper the slope (left side higher) the more confident the prediction.
